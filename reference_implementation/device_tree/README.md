Description
===========

A device tree for running the loopback pl on a Zedboard or a Picozed.
The interesting stuff is not board specific and is in `dma_loopback.dtsi`.
For other boards, simply make sure this specific bit is inserted appropriately
into the device tree.

The device tree works with the kernel driver in the accompanying 
`driver` directory.

Also included is a minimal device tree for use with the minimal (direct DMA
only) design in the `dma_loopback_pl` directory.

Build and install
=================

To build, run make.

This depends on `dtc` (device tree compiler) being installed and in your path
(`dtc` is easily available in e.g. Ubuntu).

Copy the the relevant `.dtb` file to wherever it needs to be to be properly
picked up by the boot system. In our yocto builds, this is in the boot 
partition of the SD card, with a suitable name set by `uEnv.txt`.

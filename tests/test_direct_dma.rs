use std::{
    error::Error,
    path::PathBuf,
    ptr::copy_nonoverlapping,
    sync::atomic::fence,
    sync::atomic::Ordering,
    time,
};
use rand::{
    distributions::Uniform,
    Rng,
};

use itertools::izip;

use serial_test_derive::serial;

use gannet::{
    errors::DmaError,
    uni_channel::direct,
};

pub fn run_direct_dma_and_check(
    mm2s_device: &mut direct::Device,
    s2mm_device: &mut direct::Device, src_offset: usize,
    dest_offset: usize, nbytes: usize, test_data: Vec<u8>,
    s2mm_first: bool) -> Result<(), Box<Error>> {

    // Get a pointer to and the size of the memory for each DMA device
    let (s2mm_dma_data, _s2mm_dma_data_size) = s2mm_device.get_memory();
    let (mm2s_dma_data, _mm2s_dma_data_size) = mm2s_device.get_memory();

    // Create a vector to overwrite the destination before beginning the
    // transfer
    let destination_setup_data = vec!(1u8; nbytes);
    // Create a vector for a sanity check on the destination memory
    let mut test_destination_data = vec!(0u8; nbytes);
    // Create a vector into which we can copy the data out of the destination
    // memory
    let mut destination_data = vec!(0u8; nbytes);

    // Need the memory fences here to prevent the cpu from reordering
    // the non volatile reads/writes around volatile reads/writes
    fence(Ordering::SeqCst);
    unsafe {
        // Copy the test data into the source memory
        copy_nonoverlapping(
            test_data.as_ptr(), mm2s_dma_data.offset(src_offset as isize),
            nbytes);
        // Set up the destination memory with known values so we know the
        // state of the memory before the transfer
        copy_nonoverlapping(
            destination_setup_data.as_ptr(),
            s2mm_dma_data.offset(dest_offset as isize), nbytes);
        // Copy the values out of the destination memory into the test vector
        copy_nonoverlapping(
            s2mm_dma_data.offset(dest_offset as isize),
            test_destination_data.as_mut_ptr(), nbytes);
    }
    fence(Ordering::SeqCst);

    // Sanity check to make sure the destination memory is known before the
    // transfer. This is so we know any change is cause by the DMA transfer.
    assert!(test_destination_data == destination_setup_data);

    // Trigger the DMA transfers
    if s2mm_first {
        s2mm_device.do_dma(&nbytes, &dest_offset)?;
        mm2s_device.do_dma(&nbytes, &src_offset)?;
    }
    else {
        mm2s_device.do_dma(&nbytes, &src_offset)?;
        s2mm_device.do_dma(&nbytes, &dest_offset)?;
    }

    let timeout = time::Duration::from_millis(1000);

    // Wait for DMA transfers to complete
    mm2s_device.wait_transfer_complete(&timeout)?;
    s2mm_device.wait_transfer_complete(&timeout)?;

    // Need the memory fences here to prevent the cpu from reordering
    // the non volatile reads/writes around volatile reads/writes
    fence(Ordering::SeqCst);
    unsafe {
        // Read the destination memory.
        copy_nonoverlapping(
            s2mm_dma_data.offset(dest_offset as isize),
            destination_data.as_mut_ptr(), nbytes);
    }
    fence(Ordering::SeqCst);

    // Check that we have received the correct data.
    assert!(test_data == destination_data);

    Ok(())
}

/// The system should be able to transfer data from MM2S memory to S2MM
/// memory via the PL, such that:
///
///     * 8 bytes <= transfer size < 8 Mbytes
///     * Transfer size is a multiple of 8
///
/// The system should be able to transfer data from any source location in
/// the MM2S memory to any destination location in the S2MM memory, such
/// that:
///
///     * 0 <= source location < MM2S memory size
///     * Source location is a multiple of 8
///     * 0 <= destination location < S2MM memory size
///     * Destination location is a multiple of 8
///
/// The system should be able to do multiple back to back DMA
/// transfers.
#[test]
#[serial]
fn test_direct_dma_transfers() {
    // This test uses DMA to transfer 8 bytes from the highest available
    // source offset to the highest possble destination offset. Then
    // (self.max_transfer_size - 8) bytes from source offset 0 to
    // destination offset 0. Then eight random amounts of  data from a
    // random source offset in the mm2s memory to the PL and then back into
    // a random destination offset in the s2mm memory.

    let s2mm_device_path: PathBuf = ["/dev", "axi_dma_s2mm"].iter().collect();
    let mm2s_device_path: PathBuf = ["/dev", "axi_dma_mm2s"].iter().collect();

    // Create the devices. In the loopback PL the MM2S DMA engine stream
    // output is connected to the S2MM DMA engine stream input.
    let mut s2mm_device =
        direct::Device::new(&s2mm_device_path).unwrap();
    let mut mm2s_device =
        direct::Device::new(&mm2s_device_path).unwrap();

    // Get a pointer to and the size of the memory for each DMA device
    let (_s2mm_dma_data, s2mm_dma_data_size) = s2mm_device.get_memory();
    let (_mm2s_dma_data, mm2s_dma_data_size) = mm2s_device.get_memory();

    let mut rng = rand::thread_rng();
    let range = Uniform::new(0, 1u16 << 8);

    // Test the smallest transfer size of 8 bytes into the highest memory
    // position and the largest possible transfer size into the offset 0.
    let mut sizes = vec!(8, (1 << 23) - 8);
    let mut src_offsets = vec!(mm2s_dma_data_size - 8, 0);
    let mut dest_offsets = vec!(s2mm_dma_data_size - 8, 0);

    let s2mm_first: bool = false;

    // Generate valid random values for the size and source & destination
    // offsets of the transfer. We don't use the full range here to speed the
    // test up.
    for n in 0..3 {
        sizes.push(8*rng.gen_range(1, (1 << 16)/8));
        src_offsets.push(
            8*rng.gen_range(0, (mm2s_dma_data_size - sizes[n+2])/8));
        dest_offsets.push(
            8*rng.gen_range(0, (s2mm_dma_data_size - sizes[n+2])/8));
    }

    for (nbytes, src_offset, dest_offset) in izip!(
        &sizes, &src_offsets, &dest_offsets) {

        // Generate random data
        let test_data: Vec<u8> =
            (0..*nbytes).map(|_| rng.sample(&range) as u8).collect();

        run_direct_dma_and_check(
            &mut mm2s_device, &mut s2mm_device, *src_offset, *dest_offset,
            *nbytes, test_data, s2mm_first).unwrap();
    }
}

///The system should be cache coherent after a transfer.
#[test]
#[serial]
fn test_cache_coherency() {
    // This test uses DMA to transfer data from mm2s memory to the PL and
    // then back into s2mm memory. A second DMA transfer is then performed
    // to overwrite the part of the newly transferred data with another
    // section of the original data. When the data is read from destination
    // s2mm memory, it should match the original data with one section
    // modified.
    //
    // Example:
    // ********
    // Original data in mm2s:               |__1__|__2__|__3__|__4__|
    //
    // Data in s2mm after initial transfer: |__1__|__2__|__3__|__4__|
    //
    // Second transfer just reads out section 4 and places it in section 2.
    //
    // Data in s2mm after second transfer:  |__1__|__4__|__3__|__4__|
    //
    // When the userspace application then reads the full data from the
    // s2mm memory, it should read the data correctly. Ie the cache has
    // been updated as part of the transfer.

    let s2mm_device_path: PathBuf = ["/dev", "axi_dma_s2mm"].iter().collect();
    let mm2s_device_path: PathBuf = ["/dev", "axi_dma_mm2s"].iter().collect();

    // Create the devices. In the loopback PL the MM2S DMA engine stream
    // output is connected to the S2MM DMA engine stream input.
    let mut s2mm_device =
        direct::Device::new(&s2mm_device_path).unwrap();
    let mut mm2s_device =
        direct::Device::new(&mm2s_device_path).unwrap();

    // Get a pointer to and the size of the memory for each DMA device
    let (s2mm_dma_data, s2mm_dma_data_size) = s2mm_device.get_memory();
    let (_mm2s_dma_data, mm2s_dma_data_size) = mm2s_device.get_memory();

    let mut rng = rand::thread_rng();
    let range = Uniform::new(0, 1u16 << 8);

    let s2mm_first: bool = false;

    for _n in 0..4 {

        // Randomly set the size of the transfer. We don't use the full range
        // here to speed the test up.
        let transfer_size = 8*rng.gen_range(4, (1 << 16)/8);
        // Pick random source and destination offsets
        let src_offset =
            8*rng.gen_range(0, (mm2s_dma_data_size - transfer_size)/8);
        let dest_offset =
            8*rng.gen_range(0, (s2mm_dma_data_size - transfer_size)/8);

        // Generate random test data
        let test_data: Vec<u8> =
            (0..transfer_size).map(|_| rng.sample(&range) as u8).collect();

        // Randomly set the size of the overwriting transfer and the offsets
        // for the overwriting transfer
        let overwrite_transfer_size = 8*rng.gen_range(1, transfer_size/8);
        let overwrite_src_offset = 8*rng.gen_range(
            src_offset/8,
            (src_offset + transfer_size - overwrite_transfer_size)/8);
        let overwrite_dest_offset = 8*rng.gen_range(
            dest_offset/8,
            (dest_offset + transfer_size - overwrite_transfer_size)/8);

        // Create a vector to take the expected destination data
        let mut expected_destination_data = vec!(0; transfer_size);
        expected_destination_data.copy_from_slice(&test_data);

        expected_destination_data[(overwrite_dest_offset-dest_offset)..
            (overwrite_dest_offset-dest_offset+overwrite_transfer_size)]
            .copy_from_slice(
                &test_data[(overwrite_src_offset-src_offset)..
                    (overwrite_src_offset-src_offset+overwrite_transfer_size)]);

        run_direct_dma_and_check(
            &mut mm2s_device, &mut s2mm_device, src_offset, dest_offset,
            transfer_size, test_data, s2mm_first).unwrap();

        // Trigger the DMA overwriting transfers
        mm2s_device.do_dma(
            &overwrite_transfer_size, &overwrite_src_offset).unwrap();
        s2mm_device.do_dma(
            &overwrite_transfer_size, &overwrite_dest_offset).unwrap();

        let timeout = time::Duration::from_millis(1000);

        // Wait for DMA transfers to complete
        mm2s_device.wait_transfer_complete(&timeout).unwrap();
        s2mm_device.wait_transfer_complete(&timeout).unwrap();

        // Create a vector into which we can copy the data out of the
        // destination memory
        let mut destination_data = vec!(0u8; transfer_size);

        // Need the memory fences here to prevent the cpu from reordering
        // the non volatile reads/writes around volatile reads/writes
        fence(Ordering::SeqCst);
        unsafe {
            // Read the destination memory.
            copy_nonoverlapping(
                s2mm_dma_data.offset(dest_offset as isize),
                destination_data.as_mut_ptr(), transfer_size);
        }
        fence(Ordering::SeqCst);

        // Check that we have received the correct data.
        assert!(expected_destination_data == destination_data);
    }
}

/// The system should not overwrite any addresses other than those
/// specified in the DMA transfer.
#[test]
#[serial]
fn test_direct_correct_destination_address() {
    // This test uses DMA to transfer data from mm2s memory to the PL and
    // then back into four different s2mm memory locations. Each write
    // should only affect the memory locations specified.

    let s2mm_device_path: PathBuf = ["/dev", "axi_dma_s2mm"].iter().collect();
    let mm2s_device_path: PathBuf = ["/dev", "axi_dma_mm2s"].iter().collect();

    // Create the devices. In the loopback PL the MM2S DMA engine stream
    // output is connected to the S2MM DMA engine stream input.
    let mut s2mm_device =
        direct::Device::new(&s2mm_device_path).unwrap();
    let mut mm2s_device =
        direct::Device::new(&mm2s_device_path).unwrap();

    // Get a pointer to and the size of the memory for each DMA device
    let (s2mm_dma_data, s2mm_dma_data_size) = s2mm_device.get_memory();
    let (_mm2s_dma_data, mm2s_dma_data_size) = mm2s_device.get_memory();

    let mut rng = rand::thread_rng();
    let range = Uniform::new(0, 1u16 << 8);

    let s2mm_first: bool = false;

    for _n in 0..4 {

        // Randomly set the size of the transfer. We don't use the full range
        // here to speed the test up.
        let nbytes = 8*rng.gen_range(4, (1 << 16)/8);
        // Pick random source and destination offsets
        let src_offset = 8*rng.gen_range(0, (mm2s_dma_data_size - nbytes)/8);
        let dest_offset = 8*rng.gen_range(0, (s2mm_dma_data_size - nbytes)/8);

        // Generate random data
        let test_data: Vec<u8> =
            (0..nbytes).map(|_| rng.sample(&range) as u8).collect();

        // Create a vector to take the expected destination data
        let mut expected_destination_data = vec!(0u8; s2mm_dma_data_size);
        expected_destination_data[dest_offset..(dest_offset + nbytes)]
            .copy_from_slice(&test_data);

        // Create a vector to overwrite the destination before beginning the
        // transfer
        let destination_setup_data = vec!(0u8; s2mm_dma_data_size);

        // Need the memory fences here to prevent the cpu from reordering
        // the non volatile reads/writes around volatile reads/writes
        fence(Ordering::SeqCst);
        unsafe {
            // Set up the destination memory with known values so we know the
            // state of the memory before the transfer
            copy_nonoverlapping(
                destination_setup_data.as_ptr(), s2mm_dma_data,
                s2mm_dma_data_size);
        }
        fence(Ordering::SeqCst);

        run_direct_dma_and_check(
            &mut mm2s_device, &mut s2mm_device, src_offset, dest_offset,
            nbytes, test_data, s2mm_first).unwrap();

        // Create a vector into which we can copy the data out of the
        // destination memory
        let mut destination_data = vec!(0u8; s2mm_dma_data_size);

        // Need the memory fences here to prevent the cpu from reordering
        // the non volatile reads/writes around volatile reads/writes
        fence(Ordering::SeqCst);
        unsafe {
            // Read the destination memory.
            copy_nonoverlapping(
                s2mm_dma_data, destination_data.as_mut_ptr(),
                s2mm_dma_data_size);
        }
        fence(Ordering::SeqCst);

        // Check that we have received the correct data.
        assert!(expected_destination_data == destination_data);
    }
}

/// The system should be able to set up the MM2S operation first and then the
/// S2MM operation or vice versa.
#[test]
#[serial]
fn test_direct_dma_order() {
    // This test sets up the S2MM transfer and then the MM2S transfer for the
    // first transfer. It then performs up a second transfer by setting up the
    // MM2S channel and then the S2MM channel. It then performs 8 more
    // transfers by randomly selecting which channel to set up first.

    let s2mm_device_path: PathBuf = ["/dev", "axi_dma_s2mm"].iter().collect();
    let mm2s_device_path: PathBuf = ["/dev", "axi_dma_mm2s"].iter().collect();

    // Create the devices. In the loopback PL the MM2S DMA engine stream
    // output is connected to the S2MM DMA engine stream input.
    let mut s2mm_device =
        direct::Device::new(&s2mm_device_path).unwrap();
    let mut mm2s_device =
        direct::Device::new(&mm2s_device_path).unwrap();

    // Get a pointer to and the size of the memory for each DMA device
    let (_s2mm_dma_data, s2mm_dma_data_size) = s2mm_device.get_memory();
    let (_mm2s_dma_data, mm2s_dma_data_size) = mm2s_device.get_memory();

    let mut rng = rand::thread_rng();
    let range = Uniform::new(0, 1u16 << 8);

    for n in 0..4 {

        // Alternate between performing an mm2s transfer and an s2mm transfer
        // first
        let s2mm_first =
            if n % 2 == 0 {false}
            else {true};

        // Generate valid random values for the size and source & destination
        // offsets of the transfer. We don't use the full range here to speed
        // the test up.
        let nbytes = 8*rng.gen_range(1, (1 << 16)/8);
        let src_offset = 8*rng.gen_range(0, (mm2s_dma_data_size - nbytes)/8);
        let dest_offset = 8*rng.gen_range(0, (s2mm_dma_data_size - nbytes)/8);

        // Generate random data
        let test_data: Vec<u8> =
            (0..nbytes).map(|_| rng.sample(&range) as u8).collect();

        run_direct_dma_and_check(
            &mut mm2s_device, &mut s2mm_device, src_offset, dest_offset,
            nbytes, test_data, s2mm_first).unwrap();
    }
}

/// The system should return the number of bytes that was actually
/// transferred.
///
/// In the event that a program initiates an S2MM transfer requesting more
/// data than is available in the incoming data packet, the system will
/// return when the DMA engine has transferred the full incoming data
/// packet even though this is less than the requested number of bytes.
///
/// The calling function will be expecting the amount requested so the
/// system should return the number of bytes that were actually
/// transferred.
#[test]
#[serial]
fn test_direct_dma_mismatched_lengths() {
    // This test uses DMA to transfer a random amounta of data from a
    // random source offset in the mm2s memory to the PL. It then requests
    // a larger amount of data is transferred back into a random
    // destination offset in the s2mm memory. It does this 4 times then it
    // performs one good read to ensure the DMA is still functioning.

    let s2mm_device_path: PathBuf = ["/dev", "axi_dma_s2mm"].iter().collect();
    let mm2s_device_path: PathBuf = ["/dev", "axi_dma_mm2s"].iter().collect();

    // Create the devices. In the loopback PL the MM2S DMA engine stream
    // output is connected to the S2MM DMA engine stream input.
    let mut s2mm_device =
        direct::Device::new(&s2mm_device_path).unwrap();
    let mut mm2s_device =
        direct::Device::new(&mm2s_device_path).unwrap();

    // Get a pointer to and the size of the memory for each DMA device
    let (s2mm_dma_data, s2mm_dma_data_size) = s2mm_device.get_memory();
    let (mm2s_dma_data, mm2s_dma_data_size) = mm2s_device.get_memory();

    let mut rng = rand::thread_rng();
    let range = Uniform::new(0, 1u16 << 8);

    let s2mm_first: bool = false;

    for _n in 0..4 {

        // Set the mm2s size. We don't use the full range here to speed the
        // test up.
        let mm2s_nbytes = 8*rng.gen_range(1, (1 << 16)/8);
        // Set the s2mm size. Make sure the s2mm size is larger than the mm2s.
        // We don't use the full range here to speed the test up.
        let s2mm_nbytes = 8*rng.gen_range(mm2s_nbytes/8, (1 << 18)/8);
        // Generate valid random values for source & destination offsets of
        // the transfer.
        let src_offset =
            8*rng.gen_range(0, (mm2s_dma_data_size - mm2s_nbytes)/8);
        let dest_offset =
            8*rng.gen_range(0, (s2mm_dma_data_size - s2mm_nbytes)/8);

        // Generate random data
        let test_data: Vec<u8> =
            (0..mm2s_nbytes).map(|_| rng.sample(&range) as u8).collect();

        // Set up the expected destination data. As the s2mm transfer size is
        // smaller than the mm2s transfer size we expected the mm2s size of
        // data to contain zeros at the end to make up the difference.
        let mut expected_destination_data = vec!(0u8; mm2s_nbytes);
        expected_destination_data[..mm2s_nbytes].copy_from_slice(&test_data);
        expected_destination_data.append(
            &mut vec!(0u8; s2mm_nbytes - mm2s_nbytes));

        // Create a vector to overwrite the destination before beginning the
        // transfer
        let destination_setup_data = vec!(0u8; s2mm_nbytes);

        // Need the memory fences here to prevent the cpu from reordering
        // the non volatile reads/writes around volatile reads/writes
        fence(Ordering::SeqCst);
        unsafe {
            // Copy the test data into the source memory
            copy_nonoverlapping(
                test_data.as_ptr(), mm2s_dma_data.offset(src_offset as isize),
                mm2s_nbytes);
            // Set up the destination memory with known values so we know the
            // state of the memory before the transfer
            copy_nonoverlapping(
                destination_setup_data.as_ptr(),
                s2mm_dma_data.offset(dest_offset as isize), s2mm_nbytes);
        }
        fence(Ordering::SeqCst);

        // Trigger the DMA transfers
        mm2s_device.do_dma(&mm2s_nbytes, &src_offset).unwrap();
        s2mm_device.do_dma(&s2mm_nbytes, &dest_offset).unwrap();

        let timeout = time::Duration::from_millis(1000);

        // Wait for DMA transfers to complete
        mm2s_device.wait_transfer_complete(&timeout).unwrap();

        // Check that the s2mm device returns the mm2s size even though we
        // requested a larger s2mm transfer.
        assert!(
            s2mm_device.wait_transfer_complete(&timeout).unwrap() ==
            mm2s_nbytes as u32);

        // Create a vector into which we can copy the data out of the
        // destination memory
        let mut destination_data = vec!(0u8; s2mm_nbytes);

        // Need the memory fences here to prevent the cpu from reordering
        // the non volatile reads/writes around volatile reads/writes
        fence(Ordering::SeqCst);
        unsafe {
            // Read the destination memory.
            copy_nonoverlapping(
                s2mm_dma_data.offset(dest_offset as isize),
                destination_data.as_mut_ptr(), s2mm_nbytes);
        }
        fence(Ordering::SeqCst);

        // Check that we have received the correct data.
        assert!(expected_destination_data == destination_data);
    }

    // Generate valid random values for the size and source & destination
    // offsets of the transfer. We don't use the full range here to speed
    // the test up.
    let nbytes = 8*rng.gen_range(1, (1 << 16)/8);
    let src_offset = 8*rng.gen_range(0, (mm2s_dma_data_size - nbytes)/8);
    let dest_offset = 8*rng.gen_range(0, (s2mm_dma_data_size - nbytes)/8);

    // Generate random data
    let test_data: Vec<u8> =
        (0..nbytes).map(|_| rng.sample(&range) as u8).collect();

    run_direct_dma_and_check(
        &mut mm2s_device, &mut s2mm_device, src_offset, dest_offset,
        nbytes, test_data, s2mm_first).unwrap();
}

/// The system should return `DmaError::TransferInProgress` if the user calls
/// `do_dma` multiple times without calling `wait_transfer_complete`.
/// In this case it should be possible to return to a known working state by
/// calling `reset`.
#[test]
#[serial]
fn test_direct_transfer_in_progress() {
    // This test will call do_dma twice without calling
    // wait_transfer_complete to check that the device returns a
    // TransferInProgress error. It will then call reset. It will then perform
    // another transfer to check that the device has returned to a working
    // state

    let s2mm_device_path: PathBuf = ["/dev", "axi_dma_s2mm"].iter().collect();
    let mm2s_device_path: PathBuf = ["/dev", "axi_dma_mm2s"].iter().collect();

    // Create the devices. In the loopback PL the MM2S DMA engine stream
    // output is connected to the S2MM DMA engine stream input.
    let mut s2mm_device =
        direct::Device::new(&s2mm_device_path).unwrap();
    let mut mm2s_device =
        direct::Device::new(&mm2s_device_path).unwrap();

    let mut rng = rand::thread_rng();
    let range = Uniform::new(0, 1u16 << 8);

    let nbytes = 8;
    let src_offset = 0;
    let dest_offset = 0;

    // Trigger the first transfer
    mm2s_device.do_dma(&nbytes, &src_offset).unwrap();
    s2mm_device.do_dma(&nbytes, &dest_offset).unwrap();

    // Check that the second do dma call gets the transfer in progress error
    match mm2s_device.do_dma(&nbytes, &src_offset) {
        Err(DmaError::TransferInProgress) => (),
        _ => panic!("Did not get DmaError::TransferInProgress error."),
    }
    match s2mm_device.do_dma(&nbytes, &src_offset) {
        Err(DmaError::TransferInProgress) => (),
        _ => panic!("Did not get DmaError::TransferInProgress error."),
    }

    mm2s_device.reset().unwrap();
    s2mm_device.reset().unwrap();

    // Generate random data
    let test_data: Vec<u8> =
        (0..nbytes).map(|_| rng.sample(&range) as u8).collect();

    let s2mm_first = true;

    run_direct_dma_and_check(
        &mut mm2s_device, &mut s2mm_device, src_offset, dest_offset, nbytes,
        test_data, s2mm_first).unwrap();
}

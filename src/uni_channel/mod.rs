pub mod direct;
pub mod scatter_gather;
mod registers;

use crate::{
    errors::DmaError,
    read_reg,
    TransferType,
    write_reg,
};

use registers::{
    ControlRegister,
    Registers,
    StatusRegister,
};

// This function reads the cfg registers to get the transfer type of the
// device
fn get_transfer_type (cfg: &*mut u32) -> Result<TransferType, DmaError> {

    // Read the MM2S control register
    let mm2s_ctrl_reg_val: u32 =
        read_reg(cfg, &Registers::Mm2sControlRegister.offset());
    // Read the S2MM control register
    let s2mm_ctrl_reg_val: u32 =
        read_reg(cfg, &Registers::S2mmControlRegister.offset());

    // Check if the mm2s control register exists
    let mm2s_ctrl_reg_exists = match
        mm2s_ctrl_reg_val & ControlRegister::RegisterExists.bitmask() {
            0 => false,
            _ => true,
        };

    // Check if the s2mm control register exists
    let s2mm_ctrl_reg_exists = match
        s2mm_ctrl_reg_val & ControlRegister::RegisterExists.bitmask() {
            0 => false,
            _ => true,
        };

    let transfer_type: TransferType =
        if mm2s_ctrl_reg_exists && !s2mm_ctrl_reg_exists {
            TransferType::MM2S
        }
        else if s2mm_ctrl_reg_exists && !mm2s_ctrl_reg_exists {
            TransferType::S2MM
        }
        else {
            // libgannet cannot handle the situation when we have both
            // S2MM and MM2S control registers or the situation when we
            // have neither.
            return Err(DmaError::UnsupportedCapability);
        };

    Ok(transfer_type)
}

// This function reads the cfg registers to see if the device is scatter
// gather enabled.
fn scatter_gather_enabled(cfg: &*mut u32) -> Result<bool, DmaError> {

    // Read the MM2S status register
    let mm2s_status_reg_val: u32 =
        read_reg(cfg, &Registers::Mm2sStatusRegister.offset());
    // Read the S2MM status register
    let s2mm_status_reg_val: u32 =
        read_reg(cfg, &Registers::S2mmStatusRegister.offset());

    // Check if scatter gather is enabled on the device. If it is then
    // return true else false
    if (mm2s_status_reg_val & StatusRegister::SGIncId.bitmask() != 0) ||
        (s2mm_status_reg_val & StatusRegister::SGIncId.bitmask() != 0) {
            Ok(true)
        }
    else {
        Ok(false)
    }
}

// This function reads the status register of the uni channel device and
// returns true if the device is halted and false if it is not.
fn halted(cfg: &*mut u32, transfer_type: &TransferType) -> bool {

    // Determine which status register needs to be read from depending on
    // the device capability.
    let status_reg = match transfer_type {
        TransferType::MM2S => Registers::Mm2sStatusRegister,
        TransferType::S2MM => Registers::S2mmStatusRegister,
    };

    // Read the status register
    let status_reg_val: u32 = read_reg(&cfg, &status_reg.offset());

    if status_reg_val & StatusRegister::Halted.bitmask() == 0 {
        // Device is not halted
        false
    }
    else {
        // Device is halted
        true
    }
}

// This function reads the status register of the uni channel device and
// returns true if the device is idle and false if it is not.
fn idle(cfg: &*mut u32, transfer_type: &TransferType) -> bool {

    // Determine which status register needs to be read from depending on
    // the device capability.
    let status_reg = match transfer_type {
        TransferType::MM2S => Registers::Mm2sStatusRegister,
        TransferType::S2MM => Registers::S2mmStatusRegister,
    };

    // Read the status register
    let status_reg_val: u32 = read_reg(cfg, &status_reg.offset());

    if status_reg_val & StatusRegister::Idle.bitmask() == 0 {
        // Device is not idle
        false
    }
    else {
        // Device is idle
        true
    }
}

fn clear_all_interrupts(cfg: &*mut u32, transfer_type: &TransferType) {

    // Create the bitmask to clear the status register interrupt bits
    let status_reg_irq_clear_bits: u32 =
        StatusRegister::IocIrq.bitmask() |
        StatusRegister::DlyIrq.bitmask() |
        StatusRegister::ErrIrq.bitmask();

    // Determine which status register needs to be written to depending on the
    // device capability.
    let status_reg = match transfer_type {
        TransferType::MM2S => Registers::Mm2sStatusRegister,
        TransferType::S2MM => Registers::S2mmStatusRegister,
    };

    // Write to the status register to clear the interrupt.
    write_reg(cfg, &status_reg.offset(), status_reg_irq_clear_bits);
}

// This function writes to the reset bit in the uni channel register space.
fn write_reset_bit(cfg: &*mut u32, transfer_type: &TransferType) {

    // Determine which control and status registers need to be written to
    // depending on the device capability.
    let ctrl_reg_offset = match transfer_type {
        TransferType::MM2S => Registers::Mm2sControlRegister.offset(),
        TransferType::S2MM => Registers::S2mmControlRegister.offset(),
    };

    // Reset the channel
    write_reg(cfg, &ctrl_reg_offset, ControlRegister::Reset.bitmask());
}

// This function writes to the stop bit in the uni channel register space.
fn write_stop_bit(cfg: &*mut u32, transfer_type: &TransferType) {

    // Determine which control register needs to be written to depending on
    // the device capability.
    let ctrl_reg_offset = match transfer_type {
        TransferType::MM2S => Registers::Mm2sControlRegister.offset(),
        TransferType::S2MM => Registers::S2mmControlRegister.offset(),
    };

    // Set control register to 0. This is to set the run/stop bit low which
    // halts the device.
    write_reg(cfg, &ctrl_reg_offset, 0_u32);
}

// This function reads the Status register of the DMA device and then
// returns the appropriate error code if an error has occurred or Ok if
// no error.
fn check_error(
    cfg: &*mut u32, transfer_type: &TransferType) -> Result<(), DmaError> {

    // Determine which status register needs to be read from depending on
    // the device capability.
    let status_reg = match transfer_type {
        TransferType::MM2S => Registers::Mm2sStatusRegister,
        TransferType::S2MM => Registers::S2mmStatusRegister,
    };

    // Read the status register
    let status_reg_val: u32 = read_reg(cfg, &status_reg.offset());

    // Return the error value. We only return one error value. This is not
    // ideal as it may be the case that there are multiple error types
    // set.
    if status_reg_val & StatusRegister::DmaIntErr.bitmask() != 0 {
        return Err(DmaError::Internal);
    }
    else if status_reg_val & StatusRegister::DmaSlvErr.bitmask() != 0 {
        return Err(DmaError::Slave);
    }
    else if status_reg_val & StatusRegister::DmaDecErr.bitmask() != 0 {
        return Err(DmaError::Decode);
    }
    else if status_reg_val & StatusRegister::SGIntErr.bitmask() != 0 {
        return Err(DmaError::SGInternal);
    }
    else if status_reg_val & StatusRegister::SGSlvErr.bitmask() != 0 {
        return Err(DmaError::SGSlave);
    }
    else if status_reg_val & StatusRegister::SGDecErr.bitmask() != 0 {
        return Err(DmaError::SGDecode);
    }
    else {
        Ok(())
    }
}

/// This function takes in the error codes from the DMA controller and
/// prints out the human readable form. See the description of the status
/// register in the Register Space section of the Product Guide
/// (http://www.xilinx.com/support/documentation/ip_documentation/axi_dma/v7_1/pg021_axi_dma.pdf)
/// This function was inspired by code on:
/// http://lauri.võsandi.com/hdl/zynq/xilinx-dma.html
fn print_status(cfg: &*mut u32, transfer_type: &TransferType) {

    // Determine which status register needs to be read from depending on
    // the device capability.
    let status_reg = match transfer_type {
        TransferType::MM2S => {
            println!("MM2S enabled device. Status:");
            Registers::Mm2sStatusRegister
        },
        TransferType::S2MM => {
            println!("S2MM enabled device. Status:");
            Registers::S2mmStatusRegister
        },
    };

    // Read the status register
    let status_reg_val: u32 = read_reg(cfg, &status_reg.offset());

    if status_reg_val & StatusRegister::Halted.bitmask() != 0 {
        println!(" Halted.");
    }
    else {
        println!(" Running.");
    }
    if status_reg_val & StatusRegister::Idle.bitmask() != 0 {
        println!(" Idle.");
    }
    if status_reg_val & StatusRegister::SGIncId.bitmask() != 0 {
        println!(" Scatter Gather included (SGIncld).");
    }
    if status_reg_val & StatusRegister::DmaIntErr.bitmask() != 0 {
        println!(" DMA Internal Error (DMAIntErr).");
    }
    if status_reg_val & StatusRegister::DmaSlvErr.bitmask() != 0 {
        println!(" DMA Slave Error (DMASlvErr).");
    }
    if status_reg_val & StatusRegister::DmaDecErr.bitmask() != 0 {
        println!(" DMA Decode Error (DMADecErr).");
    }
    if status_reg_val & StatusRegister::SGIntErr.bitmask() != 0 {
        println!(" Scatter Gather Internal Error (SGIntErr).");
    }
    if status_reg_val & StatusRegister::SGSlvErr.bitmask() != 0 {
        println!(" Scatter Gather Slave Err (SGSlvErr).");
    }
    if status_reg_val & StatusRegister::SGDecErr.bitmask() != 0 {
        println!(" Scatter Gather Decode Error (SGDecErr).");
    }
    if status_reg_val & StatusRegister::IocIrq.bitmask() != 0 {
        println!(" Interrupt On Complete (IOC_Irq).");
    }
    if status_reg_val & StatusRegister::DlyIrq.bitmask() != 0 {
        println!(" Interrupt On Delay (Dly_Irq).");
    }
    if status_reg_val & StatusRegister::ErrIrq.bitmask() != 0 {
        println!(" Interrupt On Error (Err_Irq).");
    }
}


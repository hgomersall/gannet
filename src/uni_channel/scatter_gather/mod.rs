pub mod descriptors;

use std::{
    cell::RefCell,
    fs::File,
    fs::OpenOptions,
    path::PathBuf,
    rc::Rc,
    time,
};

use memmap::{
    MmapMut,
};

use descriptors::{
    DESCRIPTOR_CHUNK_SIZE,
    DescriptorControlField,
    DescriptorFields,
    DESCRIPTOR_SIZE,
    DescriptorStatusField,
    DESCRIPTOR_STRIDE,
    MAX_BYTES_TRANSFER_PER_DESCRIPTOR,
    N_DESCRIPTORS_PER_CHUNK,
};

use crate::{
    check_multi_channel_device,
    DeviceSpecifics,
    errors::DmaError,
    enable_interrupts,
    get_and_consume_interrupts,
    Operation,
    read_reg,
    reset,
    setup_cfg_and_data_mem,
    setup_descriptors_mem,
    SGAndMCDeviceSpecifics,
    TransferType,
    uni_channel::check_error,
    uni_channel::clear_all_interrupts,
    uni_channel::get_transfer_type,
    uni_channel::halted,
    uni_channel::idle,
    uni_channel::print_status,
    uni_channel::registers::ControlRegister,
    uni_channel::registers::Registers,
    uni_channel::scatter_gather_enabled,
    uni_channel::write_reset_bit,
    uni_channel::write_stop_bit,
    wait_for_halted,
    wait_for_idle,
    walk_descriptor_chain_check_and_refresh,
    write_reg,
    write_regular_blocks_descriptor_chain,
};

/// The Device structure encapsulates all the information required for the
/// system to perform scatter gather DMA operations. There are methods on the
/// Device structure which enable these operations.
#[derive(Debug)]
pub struct Device {
    cfg: *mut u32,
    cfg_mem: MmapMut,
    dma_descriptors: *mut u32,
    dma_descriptors_mem: MmapMut,
    dma_descriptors_size: usize,
    dma_descriptors_phys_addr: usize,
    dma_data: *mut u8,
    dma_data_mem: MmapMut,
    dma_data_size: usize,
    dma_data_phys_addr: usize,
    transfer_type: TransferType,
    file: File,
    available_descriptor_chunks: Rc<RefCell<Vec<usize>>>,
    transfer_in_progress: bool,
}

impl Drop for Device {
    fn drop(&mut self) {
        // Reset the underlying device on drop. Ignore any errors as the
        // device is being dropped anyway.
        let _ = self.reset();
    }
}

// Implement the DeviceSpecifics trait
impl DeviceSpecifics for Device {

    fn write_reset_bit(&self) {
        write_reset_bit(&self.cfg, &self.transfer_type);
    }

    fn halted(&self) -> bool {
        halted(&self.cfg, &self.transfer_type)
    }

    fn idle(&self) -> bool {
        idle(&self.cfg, &self.transfer_type)
    }

    fn check_error(&self) -> Result<(), DmaError> {
        check_error(&self.cfg, &self.transfer_type)
    }

    fn get_and_consume_interrupts(&mut self, timeout: &time::Duration)
        -> Result<usize, DmaError> {
            get_and_consume_interrupts(&mut self.file, timeout)
        }
}

// Implement the SGAndMCDeviceSpecifics trait
impl SGAndMCDeviceSpecifics for Device {

    fn dma_data_size(&self) -> &usize {
        &self.dma_data_size
    }

    fn dma_data_phys_addr(&self) -> &usize {
        &self.dma_data_phys_addr
    }

    fn dma_descriptors(&self) -> &*mut u32 {
        &self.dma_descriptors
    }

    fn dma_descriptors_phys_addr(&self) -> &usize {
        &self.dma_descriptors_phys_addr
    }

    fn available_descriptor_chunks(&self) -> &Rc<RefCell<Vec<usize>>> {
        &self.available_descriptor_chunks
    }

    fn max_bytes_transfer_per_descriptor(&self) -> &usize {
        &MAX_BYTES_TRANSFER_PER_DESCRIPTOR
    }

    fn n_descriptors_per_chunk(&self) -> &usize {
        &N_DESCRIPTORS_PER_CHUNK
    }

    fn descriptor_stride(&self) -> &usize {
        &DESCRIPTOR_STRIDE
    }

    fn descriptor_size(&self) -> &usize {
        &DESCRIPTOR_SIZE
    }

    fn start_of_frame_bitmask(&self) -> u32 {
        DescriptorControlField::StartOfFrame.bitmask()
    }

    fn end_of_frame_bitmask(&self) -> u32 {
        DescriptorControlField::EndOfFrame.bitmask()
    }

    fn desc_field_nxt_desc_lsw_offset(&self) -> usize {
        DescriptorFields::NextDescriptorLsw.offset()
    }

    fn desc_field_nxt_desc_msw_offset(&self) -> usize {
        DescriptorFields::NextDescriptorMsw.offset()
    }

    fn desc_field_buffer_addr_lsw_offset(&self) -> usize {
        DescriptorFields::BufferAddrLsw.offset()
    }

    fn desc_field_buffer_addr_msw_offset(&self) -> usize {
        DescriptorFields::BufferAddrMsw.offset()
    }

    fn desc_field_control_offset(&self) -> usize {
        DescriptorFields::Control.offset()
    }

    // This function reads the descriptor status returns the correct error if
    // any have been reported. If there are no errors it returns the number of
    // bytes transferred by the descriptor
    fn descriptor_status(&self, descriptor_offset: usize)
        -> Result<usize, DmaError> {

            // Set the offset of the status word within the current
            // descriptor. We divide the status_offset by 4 as this offset is
            // counted in words rather than bytes.
            let status_offset =
                ((descriptor_offset + DescriptorFields::Status.offset())
                >> 2) as isize;

            // Read the descriptor status.
            let descriptor_status: u32 =
                read_reg(&self.dma_descriptors, &status_offset);

            // Check the error status of the descriptor and return the
            // relevant error.
            if DescriptorStatusField::InternalError.bitmask() &
                descriptor_status != 0 {
                    return Err(DmaError::Internal);
                }
            if DescriptorStatusField::SlaveError.bitmask() &
                descriptor_status != 0 {
                    return Err(DmaError::Slave);
                }
            if DescriptorStatusField::DecodeError.bitmask() &
                descriptor_status != 0 {
                    return Err(DmaError::Decode);
                }

            // Return the nbytes transferred by this descriptor
            Ok((DescriptorStatusField::TransferredBytes.bitmask() &
             descriptor_status) as usize)
        }

    // This function overwrites the complete bit in the descriptor status to
    // refresh it for the next run
    fn refresh_descriptor(&self, descriptor_offset: usize) {

        // Set the offset of the status word within the current
        // descriptor. We divide the status_offset by 4 as this offset is
        // counted in words rather than bytes.
        let status_offset =
            ((descriptor_offset + DescriptorFields::Status.offset())
             >> 2) as isize;

        // Overwrite the status register to clear the complete bit
        write_reg(&self.dma_descriptors, &status_offset, 0_u32);
    }

}

impl Device {

    /// A function to create the device and get it ready for use.
    ///
    /// This function should be run first before attempting any DMA transfers.
    ///
    /// In the case of failure this function will return the causing error.
    pub fn new(device_path: &PathBuf) -> Result<Device, DmaError> {

        // Initialise the device
        let mut device = Device::init(&device_path)?;

        // Reset the device
        device.reset()?;

        Ok(device)
    }

    /// A function to initiate the DMA device.
    ///
    /// Given a system file, this function extracts all the required
    /// information and returns a DMA device.
    fn init(device_path: &PathBuf) -> Result<Device, DmaError> {

        // Open the device file
        let file = OpenOptions::new()
            .read(true)
            .write(true)
            .open(&device_path)?;

        let (cfg_mem, dma_data_mem, dma_data_size, dma_data_phys_addr) =
            setup_cfg_and_data_mem(device_path, &file)?;

        // Get a raw pointer to the configuration memory
        let cfg = cfg_mem.as_ptr() as *mut u32;

        if check_multi_channel_device(&cfg)? {
            // Error if the device if a multi channel device
            return Err(DmaError::UnsupportedMultiChannel);
        }

        if !scatter_gather_enabled(&cfg)? {
            // Error if the device is not scatter gather enabled
            return Err(DmaError::UnsupportedDirect);
        }

        let transfer_type = get_transfer_type(&cfg)?;

        let (dma_descriptors_mem, dma_descriptors_size,
             dma_descriptors_phys_addr) =
            setup_descriptors_mem(device_path, &file)?;

        // Check that the dma descriptor memory space can accomodate at least
        // one descriptor chunk
        if DESCRIPTOR_CHUNK_SIZE > dma_descriptors_size {
            return Err(DmaError::InsufficientDescSpace)
        }

        // Get raw pointers to the dma data memory and the dma descriptors
        // memory
        let dma_data = dma_data_mem.as_ptr() as *mut u8;
        let dma_descriptors = dma_descriptors_mem.as_ptr() as *mut u32;

        // The descriptor space may not be a multiple of the chunk size. We
        // need to calculate the total size of the descriptor chunks which can
        // fit inside the descriptor memory space
        let dma_descriptor_chunks_size = dma_descriptors_size -
            (dma_descriptors_size % DESCRIPTOR_CHUNK_SIZE);

        // Create and populate the available descriptor chunk offsets. Rc
        // allows multiple owners of the data so that we any scatter gather
        // operations created on the device can have a reference to the
        // available_descriptor_chunks. This means that when that
        // operation is tidied up it can return its descriptor_chunks_offsets
        // to the available pool. RefCell allows the scatter gather operation
        // to mutably borrow an immutable reference.
        let available_descriptor_chunks =
            Rc::new(RefCell::new(Vec::new()));
        available_descriptor_chunks.borrow_mut()
            .extend((0..dma_descriptor_chunks_size)
                    .step_by(DESCRIPTOR_CHUNK_SIZE));

        // Create the transfer in progress flag
        let transfer_in_progress = false;

        Ok(Device {
            cfg,
            cfg_mem,
            dma_data,
            dma_data_mem,
            dma_data_size,
            dma_data_phys_addr,
            dma_descriptors,
            dma_descriptors_mem,
            dma_descriptors_size,
            dma_descriptors_phys_addr,
            transfer_type,
            file,
            available_descriptor_chunks,
            transfer_in_progress,
        })
    }

    /// Perform a reset on the device.
    ///
    /// This function will reset the DMA device, even if it is waiting for
    /// completion of a transfer.
    ///
    /// It will return `Ok` if successful or `DmaError::ResetFailed` if it is
    /// unsuccessful.
    pub fn reset(&mut self) -> Result<(), DmaError> {

        self.transfer_in_progress = false;

        reset(self)
    }

    /// A function to create a new regular blocks operation.
    ///
    /// block_size is the size of each block in the memory after transfer
    /// block_stride is the offset difference between index 0 of block n and
    ///     index 0 of block n+1 in the memory after the transfer
    /// n_blocks is the number of blocks required
    /// offset is the offset in memory of index 0 of block 0
    ///
    /// Note: n_blocks should equal the number of AXIS packets you want to
    /// read from/write to memory.
    /// Note: block_size should be greater than or equal to the size of the
    /// largest AXIS packet you want to read from/write to memory.
    pub fn new_regular_blocks_operation(
        &mut self, block_size: &usize, block_stride: &usize, n_blocks: &usize,
        offset: &usize) -> Result<Operation, DmaError> {

        // Check that the arguments are valid and write the descriptor chain
        let (n_descriptors, n_descriptor_chunks, first_descriptor_phys_addr,
        last_descriptor_phys_addr) =
            write_regular_blocks_descriptor_chain(
                self, block_size, block_stride, n_blocks, offset)?;

        // Create the new operation with the required number of descriptor
        // blocks assigned. We remove the descriptor chunks from the available
        // descriptor chunks list at the same time as creating the operation
        // so that in the event of an error they can be returned by the drop
        // function on the operation
        let operation =
            Operation::new(
                Rc::clone(&self.available_descriptor_chunks),
                self.available_descriptor_chunks.try_borrow_mut()?
                .drain(..n_descriptor_chunks).collect(), n_descriptors,
                first_descriptor_phys_addr, last_descriptor_phys_addr)?;

        Ok(operation)
    }

    /// A function to do a DMA transfer.
    /// It checks that the operation passed to it was created by this device.
    /// If not it returns a ``DmaError::WrongDevice`` error.
    ///
    /// Operations should be refreshed before being passed to this function
    /// call.
    ///
    /// This function expects:
    ///
    /// n_descriptors = n_blocks = n AXIS packet to be received
    ///
    /// For this reason it will set the DMA engine to interrupt after all
    /// descriptors on the operation have completed (ie. when the expected
    /// number of AXIS packets have been received).
    ///
    /// If successful this function returns Ok.
    pub fn do_dma(
        &mut self, operation: &Operation) -> Result<(), DmaError> {

        // Check that the device is not currently doing a transfer. If it is
        // then error
        if self.transfer_in_progress {
            return Err(DmaError::TransferInProgress);
        }

        // Check that the operation was created by this device. If not raise
        // an error.
        if !operation.same_device(&self.available_descriptor_chunks) {
            return Err(DmaError::WrongDevice);
        }

        enable_interrupts(&mut self.file)?;

        // Determine which ctrl, current descriptor and tail_descriptor
        // registers need to be used depending on the transfer type.
        let (ctrl_reg, current_desc_lsw_reg, current_desc_msw_reg,
             tail_desc_lsw_reg, tail_desc_msw_reg) = match
            self.transfer_type {
                TransferType::MM2S => (
                    Registers::Mm2sControlRegister,
                    Registers::Mm2sSGCurrentDescriptorLsw,
                    Registers::Mm2sSGCurrentDescriptorMsw,
                    Registers::Mm2sSGTailDescriptorLsw,
                    Registers::Mm2sSGTailDescriptorMsw,),
                TransferType::S2MM => (
                    Registers::S2mmControlRegister,
                    Registers::S2mmSGCurrentDescriptorLsw,
                    Registers::S2mmSGCurrentDescriptorMsw,
                    Registers::S2mmSGTailDescriptorLsw,
                    Registers::S2mmSGTailDescriptorMsw,),
            };

        // Set transfer in progress on the device
        self.transfer_in_progress = true;

        // Set the address of the first descriptor in the transfer
        write_reg(
            &self.cfg, &current_desc_lsw_reg.offset(),
            *operation.first_descriptor_phys_addr() as u32);
        write_reg(
            &self.cfg, &current_desc_msw_reg.offset(),
            (operation.first_descriptor_phys_addr() >> 32) as u32);

        // SET THE DMA CONFIG REGISTER FOR THE RUN
        // Set:
        //     run bit
        //     interrupt on complete bit
        //     interrupt on error bit
        //     Set Interrupt on Complete threshold to n_descriptors so the
        //         DMA engine waits for all descriptors to complete before
        //         sending the interrupt
        // All other config bits are set to 0, except IRQThreshold, which
        // implicitly acquires the default value of 1.
        write_reg(
            &self.cfg, &ctrl_reg.offset(),
            ControlRegister::RunStop.bitmask() |
            ControlRegister::IocIrqEn.bitmask() |
            ControlRegister::ErrIrqEn.bitmask() |
            (operation.n_descriptors <<
             ControlRegister::IrqThreshold.offset()) as u32);

        // BEGIN THE OPERATION
        // Write the address of the last descriptor. This triggers the
        // transfer.
        write_reg(
            &self.cfg, &tail_desc_lsw_reg.offset(),
            *operation.last_descriptor_phys_addr() as u32);
        write_reg(
            &self.cfg, &tail_desc_msw_reg.offset(),
            (operation.last_descriptor_phys_addr() >> 32) as u32);

        Ok(())
    }

    /// A function to wait for the completion of the transfer. First it waits
    /// for an interrupt indicating completion of the DMA then it keeps
    /// checking the device status register until the transfer finishes. It
    /// then clears the interrupt and stops the device. It will block for up
    /// to the timeout period. If the transfer does not complete by then it
    /// will return. In this case the device should be reset before another
    /// transfer is attempted.
    ///
    /// Possible errors include: DmaError::Slave, DmaError::Decode,
    /// DmaError::Internal, DmaError::SGInternal, DmaError::SGSlave,
    /// DmaError::SGDecode, DmaError::TimedOut, DmaError::WrongDevice.
    ///
    /// Note: After this function returns the operation will be in a stale
    /// state. The user must call
    /// ``dma::scatter_gather::device::check_and_refresh_operation`` after
    /// this function to refresh the operation.
    pub fn wait_transfer_complete(
        &mut self, timeout: &time::Duration) -> Result<(), DmaError> {

        // Get the time now
        let get_and_consume_interrupts_time_check = time::Instant::now();

        // An interrupt means that the DMA has completed but (in the case off
        // an MM2S transfer) the data has not necessarily been streamed out of
        // the DMA engine. The DMA engine buffers some data so the DMA can
        // complete without the data streaming out of the DMA engine. The
        // device sets the idle bit high when the data has been streamed out.
        // This is checked below.
        if self.get_and_consume_interrupts(&timeout)? == 0 {
            // Poll timed out
            return Err(DmaError::TimedOut);
        }

        // Work out how much of the timeout we have remaining after some was
        // used waiting for the interrupt.
        let timeout_used = get_and_consume_interrupts_time_check.elapsed();
        let timeout_remaining =
            if timeout_used > *timeout {time::Duration::from_secs(0)}
            else {*timeout - timeout_used};

        // Check the error status of the DMA transfer.
        self.check_error()?;

        // If we are doing an MM2S transfer we wait and check if the
        // device is idle. We need to do this as the DMA engine sends an
        // interrupt when the DMA is complete which is not necessarily
        // when the data has streamed out of the DMA engine (MM2S
        // transfers). When the device has returned to idle, it has
        // definitely finished streaming the data out.
        // S2MM transfers do not suffer from this issue as the DMA
        // finishes after the data has finished streaming in.
        match self.transfer_type {
            TransferType::MM2S => wait_for_idle(self, &timeout_remaining)?,
            TransferType::S2MM => (),
        }

        // Write to the status register to clear the interrupt.
        clear_all_interrupts(&self.cfg, &self.transfer_type);

        // Set control register to 0. This is to set the run/stop bit low
        // which halts the device.
        write_stop_bit(&self.cfg, &self.transfer_type);

        // Wait for the status register to show the device is halted. At that
        // point, we can refresh the descriptor chain.
        wait_for_halted(self, &time::Duration::from_millis(100))?;

        // Transfer has finished
        self.transfer_in_progress = false;

        Ok(())
    }

    /// A function to refresh the descriptors on an operation by overwriting
    /// the status registers with 0 thus setting the complete bit to 0.
    ///
    /// If a DMA transfer errors then `refresh_operation` can be called before
    /// attempting any more transfers. A subsequent `do_dma`
    /// call without first calling `refresh_operation` will work but there
    /// will be a lag before the transfer commences whilst the do dma function
    /// refreshes the descriptor chain.
    pub fn check_and_refresh_operation(
        &mut self, operation: &Operation) -> Result<usize, DmaError> {

        // Check that the operation was created by this device. If not raise
        // an error.
        if !operation.same_device(&self.available_descriptor_chunks) {
            return Err(DmaError::WrongDevice);
        }

        // Walk the descriptor chain, checking the status of the descriptors
        // and refreshing them.
        let nbytes =
            walk_descriptor_chain_check_and_refresh(self, &operation)?;

        Ok(nbytes)
    }


    /// A function which returns a pointer to the device memory.
    ///
    /// This function returns a tuple which includes a pointer to the device
    /// memory allowing the calling program to directly write the data for the
    /// DMA transfer or directly read the data from the DMA transfer. Along
    /// with the size of the memory space (dma_data, dma_data_size).
    pub fn get_memory(&self) -> (*mut u8, usize) {
        // We are happy to return this pointer to the calling function because
        // the underlying memory has been permanently allocated to the device
        // by the kernel (This happens in the driver).
        (self.dma_data, self.dma_data_size)
    }

    /// A function which returns the number descriptor chunks available on the
    /// device when this function is called.
    pub fn n_available_descriptor_chunks (&self) -> Result<usize, DmaError> {
        let n_avail_desc_chunks =
            self.available_descriptor_chunks.try_borrow()?.len();

        Ok(n_avail_desc_chunks)
    }

    /// A function which returns the number descriptors available on the
    /// device when this function is called.
    pub fn n_available_descriptors (&self) -> Result<usize, DmaError> {
        let n_avail_desc =
            self.available_descriptor_chunks.try_borrow()?.len()*
            N_DESCRIPTORS_PER_CHUNK;

        Ok(n_avail_desc)
    }

    /// This function prints the status of the device in human readable form.
    pub fn _print_status(&self) {
        print_status(&self.cfg, &self.transfer_type);
    }

    pub fn print_info(&self) {
        println!("{:?}", self);
    }
}

#[cfg(test)]
mod tests {
    use std::{
        path::PathBuf,
        time,
    };
    use rand::Rng;
    use serial_test_derive::serial;

    use super::{
        Device,
        DESCRIPTOR_STRIDE,
        DmaError,
        N_DESCRIPTORS_PER_CHUNK,
        MAX_BYTES_TRANSFER_PER_DESCRIPTOR,
    };

    /// The `dma::scatter_gather::device::new` function should return an
    /// UnsupportedDirect error if the underlying device is a direct DMA
    /// device.
    #[test]
    #[serial]
    fn test_unsupported_direct() {
        let device_path: PathBuf = ["/dev", "axi_dma_mm2s"].iter()
            .collect();

        let device = Device::new(&device_path);

        match device {
            Err(DmaError::UnsupportedDirect) => (),
            _ => panic!("Did not get DmaError::UnsupportedDirect error."),
        }
    }

    /// The `dma::scatter_gather::device::new` function should return an
    /// UnsupportedCapability error if the underlying device is capable of
    /// performing both MM2S and S2MM.
    #[test]
    #[serial]
    fn test_unsupported_capability() {

        let device_path: PathBuf = ["/dev", "axi_dma_sg_combined"].iter()
            .collect();

        let device = Device::new(&device_path);

        match device {
            Err(DmaError::UnsupportedCapability) => (),
            _ => panic!("Did not get DmaError::UnsupportedCapability error."),
        }
    }

    /// The `dma::scatter_gather::device::new` function should return an
    /// UnsupportedMultiChannel error if the underlying device is a multi
    /// channel device.
    #[test]
    #[serial]
    fn test_unsupported_multi_channel() {

        let device_path: PathBuf = ["/dev", "axi_dma_mc_mm2s"].iter()
            .collect();

        let device = Device::new(&device_path);

        match device {
            Err(DmaError::UnsupportedMultiChannel) => (),
            _ => panic!(
                "Did not get DmaError::UnsupportedMultiChannel error."),
        }
    }

    /// The `dma::scatter_gather::device::new_regular_blocks_operation`
    /// function should check that the `block_size` argument is in the range
    /// 8 -> 2**23 and that it is a multiple of 8. If either of these is not
    /// true then it should return a `DmaError::InvalidSize` error.
    #[test]
    #[serial]
    fn test_invalid_size() {

        // Create the device
        let device_path: PathBuf =
            ["/dev", "axi_dma_sg_mm2s"].iter().collect();
        let mut device = Device::new(&device_path).unwrap();

        let block_size: usize = loop {
            // Generate a random block_size
            let mut rng = rand::thread_rng();
            let block_size: usize = rng.gen();

            if block_size < 8 || block_size >= 1<<23 || block_size % 8 != 0 {
                // Check that block_size is invalid, if it is then return it
                break block_size;
            }
        };

        let block_stride = block_size;
        let n_blocks = 1;
        let offset: usize = 0;

        let operation = device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset);

        match operation {
            Err(DmaError::InvalidSize) => (),
            _ => panic!("Did not get DmaError::InvalidSize error."),
        }
    }

    /// The `dma::scatter_gather::device::new_regular_blocks_operation`
    /// function should check that the `offset` argument is in the range
    /// 0 -> `dma_data_size` and that it is a multiple of 8. If either of
    /// these is not true then it should return a `DmaError::InvalidOffset`
    /// error.
    #[test]
    #[serial]
    fn test_invalid_offset() {

        // Create the device
        let device_path: PathBuf =
            ["/dev", "axi_dma_sg_mm2s"].iter().collect();
        let mut device = Device::new(&device_path).unwrap();

        let block_size: usize = 16;
        let block_stride = block_size;
        let n_blocks = 1;

        let offset: usize = loop {
            // Generate a random offset
            let mut rng = rand::thread_rng();
            let offset: usize = rng.gen();

            if offset >= device.dma_data_size || offset % 8 != 0 {
                // Check that offset is invalid, if it is then return it
                break offset;
            }
        };

        let operation = device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset);

        match operation {
            Err(DmaError::InvalidOffset) => (),
            _ => panic!("Did not get DmaError::InvalidOffset error."),
        }
    }

    /// The `dma::scatter_gather::device::new_regular_blocks_operation`
    /// function should check that the `block_stride` argument is in the range
    /// block_size -> `dma_data_size` and that it is a multiple of 8. If
    /// either of these is not true then it should return a
    /// `DmaError::InvalidStride` error.
    #[test]
    #[serial]
    fn test_invalid_block_stride() {

        // Create the device
        let device_path: PathBuf =
            ["/dev", "axi_dma_sg_mm2s"].iter().collect();
        let mut device = Device::new(&device_path).unwrap();

        let block_size: usize = 1024;
        let offset: usize = 0;
        let n_blocks = 1;

        let block_stride: usize = loop {
            // Generate a random block stride
            let mut rng = rand::thread_rng();
            let block_stride: usize = rng.gen();

            if block_stride < block_size ||
                block_stride >= device.dma_data_size ||
                    block_stride % 8 != 0 {
                        // Check that block_stride is invalid, if it is then
                        // return it
                        break block_stride;
                    }
        };

        let operation = device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset);

        match operation {
            Err(DmaError::InvalidStride) => (),
            _ => panic!("Did not get DmaError::InvalidStride error."),
        }
    }

    /// The `dma::scatter_gather::device::new_regular_blocks_operation`
    /// function should check that the `n_blocks` argument does not exceed the
    /// maximum possible number of blocks given the block_stride. If it does
    /// then the function should return a `DmaError::InvalidNBlocks` error.
    #[test]
    #[serial]
    fn test_invalid_n_blocks() {

        // Create the device
        let device_path: PathBuf =
            ["/dev", "axi_dma_sg_mm2s"].iter().collect();
        let mut device = Device::new(&device_path).unwrap();

        let block_size: usize = 1024;
        let offset: usize = 0;

        // Generate a random block stride
        let mut rng = rand::thread_rng();
        let block_stride: usize =
            8*rng.gen_range(block_size/8, device.dma_data_size/8);

        let min_n_blocks = device.dma_data_size/block_stride + 1;

        let n_blocks =
            rng.gen_range(min_n_blocks, min_n_blocks*2);

        let operation = device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset);

        match operation {
            Err(DmaError::InvalidNBlocks) => (),
            _ => panic!("Did not get DmaError::InvalidNBlocks error."),
        }
    }

    /// The `dma::scatter_gather::device::new_regular_blocks_operation`
    /// function should return an InsufficientDescChunks error if the
    /// device does not have sufficient descriptor chunks available to set up
    /// the requested operation.
    #[test]
    #[serial]
    fn test_insufficient_descriptor_chunks() {

        let device_path: PathBuf = ["/dev", "axi_dma_sg_mm2s"].iter()
            .collect();

        let mut device = Device::new(&device_path).unwrap();

        let mut rng = rand::thread_rng();

        // Work out how many descriptor chunks are available on the device
        let n_descriptor_chunks_available =
            device.dma_descriptors_size/
            (N_DESCRIPTORS_PER_CHUNK * DESCRIPTOR_STRIDE);

        // Calculate the total number of available descriptors
        let total_available_descriptors =
            n_descriptor_chunks_available * N_DESCRIPTORS_PER_CHUNK;

        // Randomly select a block size which is small enough for us to set
        // n_blocks such that it will cause an insufficient descriptor chunks
        // error
        let block_size = 8*rng.gen_range(1, total_available_descriptors/8);

        // Set the block stride so that the blocks won't overlap
        let block_stride = block_size;
        let offset = 0;

        // Select the number of blocks so that the device does not have
        // enough descriptors
        let n_blocks = rng.gen_range(
            total_available_descriptors + 1,
            device.dma_data_size/block_stride + 1);

        let operation = device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset);

        match operation {
            Err(DmaError::InsufficientDescChunks) => (),
            _ => panic!(
                    "Did not get DmaError::InsufficientDescChunks error."),
        }
    }

    /// The `dma::scatter_gather::device::new_regular_blocks_operation`
    /// function should check that the `block_size`, `block_stride`,
    /// `n_blocks` and `offset` arguments will not cause a transfer to memory
    /// outside `dma_data`. If they do then it would cause a memory overflow.
    /// The `dma::device::new_regular_blocks_operation` function should
    /// prevent this by returning a `DmaError::MemoryOverflow` error.
    #[test]
    #[serial]
    fn test_memory_overflow() {

        // Create the device
        let device_path: PathBuf =
            ["/dev", "axi_dma_sg_mm2s"].iter().collect();
        let mut device = Device::new(&device_path).unwrap();

        let (block_size, block_stride, n_blocks, offset) = loop {
            // Generate a random nbytes
            let mut rng = rand::thread_rng();

            // Randomly select arguments
            let block_size =
                8*rng.gen_range(1, MAX_BYTES_TRANSFER_PER_DESCRIPTOR/8 - 1);
            let block_stride: usize =
                8*rng.gen_range(block_size/8, device.dma_data_size/8);

            let max_n_blocks = device.dma_data_size/block_stride;
            let n_blocks = rng.gen_range(1, max_n_blocks + 1);

            let offset: usize = 8*rng.gen_range(0, device.dma_data_size/8);

            if (offset + block_stride*(n_blocks-1) + block_size) >
                device.dma_data_size {
                    // Check that nbytes and offset would result in a memory
                    // overflow, if it would then return it
                    break (block_size, block_stride, n_blocks, offset);
                }
        };

        let operation = device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset);

        match operation {
            Err(DmaError::MemoryOverflow) => (),
            _ => panic!("Did not get DmaError::MemoryOverflow error."),
        }
    }

    /// The `dma::scatter_gather::device::new_regular_blocks_operation`
    /// function should check that the device is halted before modifying the
    /// descriptors. If the device is not halted, they should return a
    /// `DmaError::DeviceNotHalted` error.
    #[test]
    #[serial]
    fn test_new_operation_device_not_halted() {

        let block_size: usize = 4096;
        let block_stride = block_size;
        let offset: usize = 0;
        let n_blocks = 1;

        // Set up device paths
        let mm2s_device_path: PathBuf =
            ["/dev", "axi_dma_sg_mm2s"].iter().collect();
        let s2mm_device_path: PathBuf =
            ["/dev", "axi_dma_sg_s2mm"].iter().collect();

        // Create devices
        let mut mm2s_device = Device::new(&mm2s_device_path).unwrap();
        let mut s2mm_device = Device::new(&s2mm_device_path).unwrap();

        // Create the operations
        let mm2s_operation = mm2s_device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset).unwrap();
        let s2mm_operation = s2mm_device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset).unwrap();

        // Trigger operation
        mm2s_device.do_dma(&mm2s_operation).unwrap();
        // Need to perform the S2MM operation as well to drain the FIFO
        s2mm_device.do_dma(&s2mm_operation).unwrap();

        // We have not called wait for complete so mm2s_device should still be
        // running which should cause the expected error
        let new_operation_result = mm2s_device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset);

        let timeout = time::Duration::from_millis(100);

        // Wait for the transfers to finish
        mm2s_device.wait_transfer_complete(&timeout).unwrap();
        s2mm_device.wait_transfer_complete(&timeout).unwrap();

        match new_operation_result {
            Err(DmaError::DeviceNotHalted) => (),
            _ => panic!("Did not get DmaError::DeviceNotHalted error."),
        }
    }

    /// The `dma::scatter_gather::device::refresh_operation` function
    /// should check that the device is halted before modifying the
    /// descriptors. If the device is not halted, it should return a
    /// `DmaError::DeviceNotHalted` error.
    #[test]
    #[serial]
    fn test_refresh_operation_device_not_halted() {

        let block_size: usize = 4096;
        let block_stride = block_size;
        let offset: usize = 0;
        let n_blocks = 1;

        // Set up device paths
        let mm2s_device_path: PathBuf =
            ["/dev", "axi_dma_sg_mm2s"].iter().collect();
        let s2mm_device_path: PathBuf =
            ["/dev", "axi_dma_sg_s2mm"].iter().collect();

        // Create devices
        let mut mm2s_device = Device::new(&mm2s_device_path).unwrap();
        let mut s2mm_device = Device::new(&s2mm_device_path).unwrap();

        // Create the operations
        let mm2s_operation = mm2s_device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset).unwrap();
        let s2mm_operation = s2mm_device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset).unwrap();

        // Trigger operation
        mm2s_device.do_dma(&mm2s_operation).unwrap();
        // Need to perform the S2MM operation as well to drain the FIFO
        s2mm_device.do_dma(&s2mm_operation).unwrap();

        // We have not called wait for complete so device should still be
        // running which should cause the expected error
        let refresh_operation_result =
            mm2s_device.check_and_refresh_operation(&mm2s_operation);

        let timeout = time::Duration::from_millis(100);

        // Wait for the transfers to finish
        mm2s_device.wait_transfer_complete(&timeout).unwrap();
        s2mm_device.wait_transfer_complete(&timeout).unwrap();

        match refresh_operation_result {
            Err(DmaError::DeviceNotHalted) => (),
            _ => panic!("Did not get DmaError::DeviceNotHalted error."),
        }
    }

    /// The `dma::scatter_gather::device::do_dma` function
    /// should check that the device is not already performing a transfer. If
    /// it is then it should return `DmaError::TransferInProgress`.
    #[test]
    #[serial]
    fn test_transfer_in_progress() {

        let block_size: usize = 4096;
        let block_stride = block_size;
        let offset: usize = 0;
        let n_blocks = 1;

        // Set up device paths
        let mm2s_device_path: PathBuf =
            ["/dev", "axi_dma_sg_mm2s"].iter().collect();
        let s2mm_device_path: PathBuf =
            ["/dev", "axi_dma_sg_s2mm"].iter().collect();

        // Create devices
        let mut mm2s_device = Device::new(&mm2s_device_path).unwrap();
        let mut s2mm_device = Device::new(&s2mm_device_path).unwrap();

        // Create the operations
        let mm2s_operation = mm2s_device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset).unwrap();
        let s2mm_operation = s2mm_device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset).unwrap();

        // Trigger operation
        mm2s_device.do_dma(&mm2s_operation).unwrap();
        // Need to perform the S2MM operation as well to drain the FIFO
        s2mm_device.do_dma(&s2mm_operation).unwrap();

        // Try to trigger a second DMA transfer without calling
        // wait_transfer_complete or reset. This should return
        // TransferInProgress
        let do_dma_result =
            mm2s_device.do_dma(&mm2s_operation);

        let timeout = time::Duration::from_millis(100);

        // Wait for the transfers to finish
        mm2s_device.wait_transfer_complete(&timeout).unwrap();
        s2mm_device.wait_transfer_complete(&timeout).unwrap();

        match do_dma_result {
            Err(DmaError::TransferInProgress) => (),
            _ => panic!("Did not get DmaError::TransferInProgress error."),
        }
    }

    /// The `dma::scatter_gather::device::do_dma` function
    /// should check that the `operation` argument was created by the device
    /// which is being called on to run the DMA. If it was not then it should
    /// return a `DmaError::WrongDevice` error.
    #[test]
    #[serial]
    fn test_do_dma_wrong_device() {

        let block_size: usize = 1024;
        let block_stride = block_size;
        let offset: usize = 0;
        let n_blocks = 1;

        // Create device 0
        let device_0_path: PathBuf =
            ["/dev", "axi_dma_sg_mm2s"].iter().collect();
        let mut device_0 = Device::new(&device_0_path).unwrap();

        // Create device 1
        let device_1_path: PathBuf =
            ["/dev", "axi_dma_sg_s2mm"].iter().collect();
        let mut device_1 = Device::new(&device_1_path).unwrap();

        let operation_1 = device_1.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset).unwrap();

        // Try to run do dma on an operation which was not created by the
        // device being called
        let do_dma_result = device_0.do_dma(&operation_1);

        match do_dma_result {
            Err(DmaError::WrongDevice) => (),
            _ => panic!("Did not get DmaError::WrongDevice error."),
        }
    }

    /// The `dma::scatter_gather::device::wait_transfer_complete` function
    /// should return `DmaError::TimedOut` if it does not receive an interrupt
    /// within the `timeout` period
    #[test]
    #[serial]
    fn test_wait_transfer_complete_timeout() {

        // Create device
        let device_path: PathBuf =
            ["/dev", "axi_dma_sg_s2mm"].iter().collect();
        let mut device = Device::new(&device_path).unwrap();

        let timeout = time::Duration::from_millis(100);

        // We have not triggered a transfer so wait_transfer_complete should
        // return DmaError::TimedOut
        let wait_transfer_complete_result =
            device.wait_transfer_complete(&timeout);

        match wait_transfer_complete_result {
            Err(DmaError::TimedOut) => (),
            _ => panic!("Did not get DmaError::TimedOut error."),
        }
    }

    /// The `dma::scatter_gather::device::check_and_refresh_operation`
    /// function should check that the `operation` argument was created by the
    /// device upon which it is being called. If it was not then it should
    /// return a `DmaError::WrongDevice` error.
    #[test]
    #[serial]
    fn test_check_and_refresh_operation_wrong_device() -> Result<(), String> {

        let block_size: usize = 1024;
        let block_stride = block_size;
        let offset: usize = 0;
        let n_blocks = 1;

        // Create device 0
        let device_0_path: PathBuf =
            ["/dev", "axi_dma_sg_mm2s"].iter().collect();
        let mut device_0 = Device::new(&device_0_path).unwrap();

        // Create device 1
        let device_1_path: PathBuf =
            ["/dev", "axi_dma_sg_s2mm"].iter().collect();
        let mut device_1 = Device::new(&device_1_path).unwrap();

        let operation_1 = device_1.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset).unwrap();

        // Try to call refresh_operation on an operation which was not
        // created by the device being called
        let refresh_operation_result =
            device_0.check_and_refresh_operation(&operation_1);

        match refresh_operation_result {
            Err(DmaError::WrongDevice) => (),
            _ => panic!("Did not get DmaError::WrongDevice error."),
        }
    }
}

#define _POSIX_C_SOURCE 200112L

#include <stdio.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include <assert.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <error.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
#include <poll.h>
#include <signal.h>

#include "dma.h"
#include "gannet.h"

static void print_status(uint32_t status);
/*
 * This function takes in the error codes from the DMA controller and prints
 * out the human readable form. See the description of the status register
 * in the Register Space section of the Product Guide (http://www.xilinx.com/support/documentation/ip_documentation/axi_dma/v7_1/pg021_axi_dma.pdf)
 * This function was inspired by code on:
 * http://lauri.võsandi.com/hdl/zynq/xilinx-dma.html
 */
static void print_status(uint32_t status) {
    printf("status: %d:", status);
    if (status & DMASR_HALTED) printf(" Halted."); else printf(" Running.");
    if (status & DMASR_IDLE) printf(" Idle.");
    if (status & DMASR_SGIncId) printf(" Scatter Gather included (SGIncld).");
    if (status & DMASR_DMAIntErr) printf(" DMA Internal Error (DMAIntErr).");
    if (status & DMASR_DMASlvErr) printf(" DMA Slave Error (DMASlvErr).");
    if (status & DMASR_DMADecErr) printf(" DMA Decode Error (DMADecErr).");
    if (status & DMASR_SGIntErr) printf(" Scatter Gather Internal Error (SGIntErr).");
    if (status & DMASR_SGSlvErr) printf(" Scatter Gather Slave Err (SGSlvErr).");
    if (status & DMASR_SGDecErr) printf(" Scatter Gather Decode Error (SGDecErr).");
    if (status & DMASR_IOC_Irq) printf(" Interrupt On Complete (IOC_Irq).");
    if (status & DMASR_Dly_Irq) printf(" Interrupt On Delay (Dly_Irq).");
    if (status & DMASR_Err_Irq) printf(" Interrupt On Error (Err_Irq).");
    printf("\n");

}

static void dma_status(dma_device_t *dma_device);
/*
 * This function reads the Status register of the DMA device and then calls
 * the print_status"()" to print it out in human readable form.
 */
static void dma_status(dma_device_t *dma_device) {
    // If MM2S is enabled then print out the status otherwise print that it is
    // disabled.
    if ((dma_device->dma_device_capability == MM2S_ENABLED) ||
            (dma_device->dma_device_capability == MM2S_S2MM_ENABLED)) {
        printf("MM2S ");
        print_status(dma_device->cfg[MM2S_STATUS_REGISTER]);
    }
    else {
        printf("MM2S status: Disabled\n");
    }
    // If S2MM is enabled then print out the status otherwise print that it is
    // disabled.
    if ((dma_device->dma_device_capability == S2MM_ENABLED) ||
            (dma_device->dma_device_capability == MM2S_S2MM_ENABLED)) {
        printf("S2MM ");
        print_status(dma_device->cfg[S2MM_STATUS_REGISTER]);
    }
    else {
        printf("S2MM status: Disabled\n");
    }
}

static int dma_error_check(dma_device_t *dma_device,
        dma_direction dma_transfer_direction);
/*
 * This function reads the Status register of the DMA device and then returns
 * the appropriate error code if an error has occurred or a 0 if no error.
 * Return values   | Meaning
 * --------------- | ----------------------------------------------------------------------------------------------------------------------------
 * 0               | No error.
 * GANNET_E_DMAINT | DMA Internal Error. Buffer Length is 0 or Memory write error or incoming packet is bigger than value in the length register.
 * GANNET_E_SLAVE  | DMA Slave Error.
 * GANNET_E_DECDE  | DMA Decode Error. Address requested is invalid.
 */
static int dma_error_check(dma_device_t *dma_device,
        dma_direction dma_transfer_direction) {
    if (dma_transfer_direction == MM2S_TRANSFER) {
        /* If checking MM2S then check the status. */
        if (dma_device->cfg[MM2S_STATUS_REGISTER] & DMASR_DMASlvErr) {
            /* DMA Slave Error. */
            return GANNET_E_SLAVE;
        }
        else if (dma_device->cfg[MM2S_STATUS_REGISTER] & DMASR_SGDecErr) {
            /* DMA Decode Error. Address requested is invalid. */
            return GANNET_E_DECDE;
        }
        else
            return 0;
    }
    else {
        /* If checking S2MM then check the status. */
        if (dma_device->cfg[S2MM_STATUS_REGISTER] & DMASR_DMAIntErr) {
            /*
             * DMA Internal Error. Buffer Length is 0 or Memory write error or
             * incoming packet is bigger than value in the length register.
             */
            return GANNET_E_DMAINT;
        }
        else if (dma_device->cfg[S2MM_STATUS_REGISTER] & DMASR_DMASlvErr) {
            /* DMA Slave Error. */
            return GANNET_E_SLAVE;
        }
        else if (dma_device->cfg[S2MM_STATUS_REGISTER] & DMASR_SGDecErr) {
            /* DMA Decode Error. Address requested is invalid. */
            return GANNET_E_DECDE;
        }
        else
            return 0;
    }
}


/*
 * This function should be called when the program has finished with the DMA
 * device. For every gannet_device_init there should be one
 * gannet_device_cleanup.
 *
 * Call this function with a pointer to the DMA device structure passed as an
 * argument. It will unmap the memory and close the device file which were
 * mapped and opened as part of gannet_device_init.
 *
 * Return values     | Meaning
 * ----------------- | ------------------------------------
 * 0                 | Success
 * Other             | Error (Code passed on from munmap)
 */

int gannet_device_cleanup(dma_device_t *dma_device) {

    int munmap_res;

    // Check the device capability and perform the appropriate reset
    if (dma_device->dma_device_capability == MM2S_ENABLED) {
        /* Reset the channel */
        gannet_dma_reset(dma_device, MM2S_TRANSFER);
    }
    else if (dma_device->dma_device_capability == S2MM_ENABLED) {
        /* Reset the channel */
        gannet_dma_reset(dma_device, S2MM_TRANSFER);
    }
    else {
        /* Reset the channel */
        gannet_dma_reset(dma_device, MM2S_TRANSFER);
        gannet_dma_reset(dma_device, S2MM_TRANSFER);
    }

    if (dma_device != NULL) {
        if (dma_device->cfg != MAP_FAILED) {
            // Unmap the configuration registers
            munmap_res =
                munmap((void *) dma_device->cfg, dma_device->cfg_size);
            if (munmap_res != 0) {
                // Check result and return error if necessary
                perror("Error un-mmapping the axi cfg");
                return munmap_res;
            }
        }
        if (dma_device->cma != MAP_FAILED) {
            // Unmap the CMA memory
            munmap_res = munmap(dma_device->cma, dma_device->cma_size);
            if (munmap_res != 0) {
                // Check result and return error if necessary
                perror("Error un-mmapping the axi cma");
                return munmap_res;
            }
        }
        close(dma_device->fd);

        // Destroy the mutexes
        pthread_mutex_destroy(&dma_device->s2mm_cfg_mutex);
        pthread_mutex_destroy(&dma_device->mm2s_cfg_mutex);

        // free memory
        free(dma_device);
    }
    return 0;
}

/*
 * A function to initiate the DMA device. Given a system file, this function
 * extracts all the required information and returns a pointer to a
 * DMA device structure.
 */
dma_device_t* gannet_device_init(const char *device_path) {
    char sys_path[320];
    char uio_name[256];

    ssize_t len = 0;

    FILE *cfg_size_fp;
    FILE *cma_size_fp;
    FILE *cma_phys_addr_fp;

    // Create a pointer to a dma_device structure. Allocate the dma_device
    // structure some memory.
    dma_device_t *dma_device = calloc(1, sizeof(dma_device_t));

    // Check that the memory allocate has worked.
    libgannet_check_malloc(dma_device, fail);

    if (pthread_mutex_init(&dma_device->s2mm_cfg_mutex, NULL) != 0){
        goto fail;
    }
    if (pthread_mutex_init(&dma_device->mm2s_cfg_mutex, NULL) != 0){
        goto fail;
    }

    // Set the memory pointers to failed so that the cleanup operation cleans
    // up gracefully even if the cfg and cma haven't yet been mapped.
    dma_device->cfg = MAP_FAILED;
    dma_device->cma = MAP_FAILED;

    dma_device->fd = open(device_path, O_RDWR);

    // Use the symlink and find the name of the underlying device.
    if ((len = readlink(device_path, uio_name, sizeof(uio_name) - 1)) == -1) {
        perror("Error cannot resolve uio driver filename");
        goto fail;
    }
    // Append a \0 to the name of the underlying device
    uio_name[len] = '\0';

    // Looking up the size of the configuration register space. This is
    // defined in the device tree.
    sprintf(sys_path, "%s%s%s%s", UIO_PATH, uio_name, CFG_PATH, SIZE_PATH);
    cfg_size_fp = fopen(sys_path, "r");
    if (cfg_size_fp == NULL) {
		perror("ERROR could not open cfg size file\n");
        goto fail;
    }
    if (fscanf(cfg_size_fp, "0x%08x", &dma_device->cfg_size) != 1) {
		perror("ERROR could not read *cfg* size\n");
        goto fail;
    }
    fclose(cfg_size_fp);

    // configuration bus memory map
    dma_device->cfg = (uint32_t *) mmap(NULL, dma_device->cfg_size,
            PROT_READ | PROT_WRITE, MAP_SHARED, dma_device->fd, 0);

    if (dma_device->cfg == MAP_FAILED) {
        perror("ERROR <Memory Map Failed> could not mmap *cfg* memory\n");
        goto fail;
    }

    // Looking up the size of the CMA memory space. This is defined in the
    // device tree.
    sprintf(sys_path, "%s%s%s%s", UIO_PATH, uio_name, CMA_PATH, SIZE_PATH);
    cma_size_fp = fopen(sys_path, "r");
    if (cma_size_fp == NULL) {
		perror("ERROR could not open cma size file\n");
        goto fail;
    }
    if (fscanf(cma_size_fp, "0x%08x", &dma_device->cma_size) != 1) {
		perror("ERROR could not read *cma* size\n");
        goto fail;
    }
    fclose(cma_size_fp);

    // cma array memory map
    dma_device->cma = mmap(NULL, dma_device->cma_size, PROT_READ | PROT_WRITE,
            MAP_SHARED, dma_device->fd, sysconf(_SC_PAGESIZE));

    if (dma_device->cma == MAP_FAILED) {
        perror("ERROR <Memory Map Failed> could not mmap *cma* memory\n");
        goto fail;
    }

    // Looking up the physical address of the CMA memory space. This is
    // allocated by the kernel
    sprintf(sys_path, "%s%s%s%s", UIO_PATH, uio_name, CMA_PATH, ADDR_PATH);
    cma_phys_addr_fp = fopen(sys_path, "r");
    fscanf(cma_phys_addr_fp, "0x%08x", &dma_device->cma_phys_addr);
    fclose(cma_phys_addr_fp);

    // Check if the dma controller can do MM2S (0), S2MM (1) or both (2).
    // Check by reading the control register. The control register will read
    // 0x0 if disabled and 0x10002 if if is enabled.
    if ((dma_device->cfg[MM2S_CONTROL_REGISTER] & DMACR_REGISTER_EXISTS) &&
            !(dma_device->cfg[S2MM_CONTROL_REGISTER] & DMACR_REGISTER_EXISTS)) {

        dma_device->dma_device_capability = MM2S_ENABLED;
        // Reset the channel.
        gannet_dma_reset(dma_device, MM2S_TRANSFER);

    }
    else if ((dma_device->cfg[S2MM_CONTROL_REGISTER] & DMACR_REGISTER_EXISTS) &&
            !(dma_device->cfg[MM2S_CONTROL_REGISTER] & DMACR_REGISTER_EXISTS)) {
        dma_device->dma_device_capability = S2MM_ENABLED;
        // Reset the channel.
        gannet_dma_reset(dma_device, S2MM_TRANSFER);
    }
    else if ((dma_device->cfg[MM2S_CONTROL_REGISTER] & DMACR_REGISTER_EXISTS) &&
             (dma_device->cfg[S2MM_CONTROL_REGISTER] & DMACR_REGISTER_EXISTS)) {
        dma_device->dma_device_capability = MM2S_S2MM_ENABLED;
        // Reset the channels.
        gannet_dma_reset(dma_device, MM2S_TRANSFER);
        gannet_dma_reset(dma_device, S2MM_TRANSFER);
    }
    else
        // Unknown device capability
        goto fail;

    return dma_device;

fail:
    gannet_device_cleanup(dma_device);
    return NULL;
}

/*
 * A function to do a DMA transfer.
 * It checks that the arguments passed to it are valid and returns errors if
 * not.
 * Return values     | Meaning
 * ----------------- | ------------------------------------------------------------------------
 * 0                 | Success
 * GANNET_E_INSIZ    | Invalid nbytes (not a multiple of 8 in range 8 -> 1**23 -1)
 * GANNET_E_INOFF    | Invalid offset (not a multiple of 8 in the memory address range)
 * GANNET_E_INDIR    | Invalid direction (The DMA device does not support the direction)
 * GANNET_E_MEMOVER  | Combination of offset and nbytes will cause transfer to overrun the end of the memory
 *
 * The function writes to the DMA cfg registers in the to set up the transfer.
 * Note that the function writes to the length register last. This is
 * important as it is this write which triggers the transfer and all other cfg
 * registers must have been written to beforehand.
 *
 * The function gets the lock before performing any writes to the cfg
 * registers and releases the lock when it has finished.
 */
int gannet_do_dma(dma_device_t *dma_device,
        dma_direction dma_transfer_direction, uintptr_t offset,
        uint32_t nbytes) {

    int32_t write_res;

    int32_t irq_en = 1;
    dma_register control_reg;
    dma_register address_msw_reg;
    dma_register address_lsw_reg;
    dma_register length_reg;

    // Check that the transfer size is valid
    if ((nbytes < 8) || (nbytes >= (1<<23)) || ((nbytes % 8) != 0))
        return GANNET_E_INSIZ;

    // Check that the offset is valid
    if ((offset < 0) ||
            (offset >= dma_device->cma_size) ||
            ((offset % 8) != 0))
        return GANNET_E_INOFF;

    // Check that the device is capable of the DMA transfer direction
    // requested
    if ((dma_transfer_direction == MM2S_TRANSFER) &&
            ((dma_device->dma_device_capability != MM2S_ENABLED) &&
             (dma_device->dma_device_capability != MM2S_S2MM_ENABLED)))
        return GANNET_E_INDIR;
    if ((dma_transfer_direction == S2MM_TRANSFER) &&
            ((dma_device->dma_device_capability != S2MM_ENABLED) &&
             (dma_device->dma_device_capability != MM2S_S2MM_ENABLED)))
        return GANNET_E_INDIR;

    // Check that the combination of offset and nbytes will not cause a memory
    // overrun
    if ((offset + nbytes) >= dma_device->cma_size)
        return GANNET_E_MEMOVER;

    if (dma_transfer_direction == MM2S_TRANSFER) {
        // Get the lock to lock out the volatile cfg registers.
        pthread_mutex_lock(&dma_device->mm2s_cfg_mutex);

        // Set the cfg registers for the correct direction of
        // transfer
        control_reg = MM2S_CONTROL_REGISTER;
        address_msw_reg = MM2S_START_ADDRESS_MSW;
        address_lsw_reg = MM2S_START_ADDRESS_LSW;
        length_reg = MM2S_LENGTH;
    }
    else {
        // Get the lock to lock out the volatile cfg registers.
        pthread_mutex_lock(&dma_device->s2mm_cfg_mutex);

        // Set the cfg registers for the correct direction of
        // transfer
        control_reg = S2MM_CONTROL_REGISTER;
        address_msw_reg = S2MM_DEST_ADDRESS_MSW;
        address_lsw_reg = S2MM_DEST_ADDRESS_LSW;
        length_reg = S2MM_LENGTH;
    }

    // Enable interrupts in the uio driver
    write_res = write(dma_device->fd, &irq_en, 4);
    if (write_res < 0) {
        perror("ERROR <Interrupt Enable Failed, error writing to the character device>\n");
        exit(write_res);
    }

    // fsync is required to make sure the data is written to device. Without
    // fsync it is possible to get race conditions whereby the interrupt
    // returns before the device file interrupts are enabled.
    fsync(dma_device->fd);

    // SET THE DMA CONFIG REGISTER FOR THE RUN
    // Set:
    //     run bit
    //     interrupt on complete bit
    //     interrupt on error bit
    // All other config bits are set to 0, except IRQThreshold, which
    // implicitly acquires the default value of 1.
    //
    dma_device->cfg[control_reg] = (
            DMACR_RUN_STOP | DMACR_IOC_IrqEn | DMACR_Err_IrqEn);
    dma_device->cfg[address_msw_reg] = 0x00000000;
    dma_device->cfg[address_lsw_reg] =
        (dma_device->cma_phys_addr + offset);

    // BEGIN THE RUN
    // Write the number of bytes to transfer. This initiates the
    // transfer.
    dma_device->cfg[length_reg] = nbytes;

    // Unlock
    if (dma_transfer_direction == MM2S_TRANSFER)
        pthread_mutex_unlock(&dma_device->mm2s_cfg_mutex);
    else
        pthread_mutex_unlock(&dma_device->s2mm_cfg_mutex);

    return 0;
}

/*
 * A function to wait for the interrupt indicating completion of the transfer.
 * It then clears the interrupt and resets the channel.
 *
 * The timeout argument is used for the poll() function. poll() shall wait at
 * least timeout milliseconds for an event to occur on any of the selected
 * file descriptors. If the value of timeout is 0, poll() shall return
 * immediately. If the value of timeout is -1, poll() shall block until a
 * requested event occurs or until the call is interrupted.
 *
 *
 * Return values   | Meaning
 * --------------- | ----------------------------------------------------------------------------------------------------------------------------
 * 0               | The DMA timed out. It did not complete and the memory contents are undefined. It is up the calling code to reset the engine if necessary.
 * +ve integer     | The number of bytes transferred.
 * GANNET_E_DMAINT | DMA Internal Error. Buffer Length is 0 or Memory write error or incoming packet is bigger than value in the length register.
 * GANNET_E_SLAVE  | DMA Slave Error.
 * GANNET_E_DECDE  | DMA Decode Error. Address requested is invalid.
 * GANNET_E_INTR   | A system interrupt occurred during polling. In this case, the DMA engine is reset.
 */

int gannet_dma_interrupt_wait(dma_device_t *dma_device,
        dma_direction dma_transfer_direction, int32_t timeout_msec)
{

    // Check the direction is valid first.
    if ((dma_transfer_direction == MM2S_TRANSFER) &&
            ((dma_device->dma_device_capability != MM2S_ENABLED) &&
             (dma_device->dma_device_capability != MM2S_S2MM_ENABLED)))
        return GANNET_E_INDIR;
    if ((dma_transfer_direction == S2MM_TRANSFER) &&
            ((dma_device->dma_device_capability != S2MM_ENABLED) &&
             (dma_device->dma_device_capability != MM2S_S2MM_ENABLED)))
        return GANNET_E_INDIR;

    int32_t poll_res;
    int32_t dma_res;
    int32_t interrupt_wait_res;

    ssize_t interrupt_buffer;

    struct pollfd fds[1];
    fds[0].fd = dma_device->fd;
    fds[0].events = POLLIN;
    fds[0].revents = 0;

    /*
     * The device file descriptor will block until the DMA transfer has
     * completed. Use poll() to wait until it is no longer blocking when it
     * will return the total number of ready file descriptors. A positive
     * value is the total number of file descriptors that have been selected.
     * In this case it can only be one as we are only passing poll a single
     * file descriptor. ie that file is ready to read without blocking. A zero
     * means the poll timed out. If poll() errors it will return -1 and set
     * errno.
     */
    poll_res = poll(fds, 1, timeout_msec);

    /* Check that the DMA transfer did not error */
    dma_res = dma_error_check(dma_device, dma_transfer_direction);

    if (dma_res < 0) {
        /* The DMA transfer encountered an error. Return the error. */
        interrupt_wait_res = dma_res;

        // Reset the engine
        gannet_dma_reset(dma_device, dma_transfer_direction);

    } else if (poll_res < 0) {
        int errsv = errno;

        /* Poll has encountered an error */
        if (errsv == EINTR){
            // System interrupt should be handled at the calling level
            return GANNET_E_INTR;
        } else {
            perror("ERROR <Interrupt poll Failed>\n");
            exit(poll_res);
        }
    } else if (poll_res == 0) {
        /*
         * The device file descriptor blocked for longer than the timeout
         * period.
         */
        interrupt_wait_res = poll_res;

    } else {
        /*
         * The device file descriptor is no longer blocking indicating that
         * the DMA transfer has completed. Return the number of bytes
         * transferred.
         *
         * The read() is necessary to update the device file. This read()
         * updates the file so that next time this function is called on the
         * device, poll() will block until an interrupt is received. Without
         * this read() poll() will return immediately as the file has not
         * changed.
         */
        read(dma_device->fd, &interrupt_buffer, 4);

        if (dma_transfer_direction == MM2S_TRANSFER) {
            /*
             * Read the MM2S length register to see how many bytes were
             * transferred.
             */
            interrupt_wait_res = dma_device->cfg[MM2S_LENGTH];

            /*
             * Write to the status register to clear the interrupt. We do this
             * rather than resetting the DMA engine because the interrupt can
             * return before the DMA engine has output the data over it's
             * stream interface. A reset would therefore stop the stream
             * output before it has completed.
             */
            dma_device->cfg[MM2S_STATUS_REGISTER] = DMASR_IOC_Irq;
        } else {
            /*
             * Read the S2MM length register to see how many bytes were
             * transferred.
             */
            interrupt_wait_res = dma_device->cfg[S2MM_LENGTH];

            /*
             * Write to the status register to clear the interrupt
             */
            dma_device->cfg[S2MM_STATUS_REGISTER] = DMASR_IOC_Irq;
        }
    }

    return interrupt_wait_res;
}

/*
 * Return a pointer to the device CMA memory.
 */
void* gannet_get_memory(dma_device_t *dma_device) {
    /* Return a pointer to the dma_device->cma. */
    return dma_device->cma;
}

/*
 * Perform a reset on the device.
 */
int  gannet_dma_reset(dma_device_t *dma_device,
        dma_direction dma_transfer_direction) {

    int32_t reset_delay_count = 0;
    /* Set the maximum number of reset sleeps. */
    int32_t max_reset_delay = 1000000;

    /* Set the time period for the reset sleeps. */
    struct timespec reset_sleep;
    reset_sleep.tv_sec = 0;
    reset_sleep.tv_nsec = 100;

    int32_t poll_res;
    ssize_t interrupt_buffer;

    struct pollfd fds[1];
    fds[0].fd = dma_device->fd;
    fds[0].events = POLLIN;
    fds[0].revents = 0;

    // Check the direction is valid first.
    if ((dma_transfer_direction == MM2S_TRANSFER) &&
            ((dma_device->dma_device_capability != MM2S_ENABLED) &&
             (dma_device->dma_device_capability != MM2S_S2MM_ENABLED)))
        return GANNET_E_INDIR;
    if ((dma_transfer_direction == S2MM_TRANSFER) &&
            ((dma_device->dma_device_capability != S2MM_ENABLED) &&
             (dma_device->dma_device_capability != MM2S_S2MM_ENABLED)))
        return GANNET_E_INDIR;

    if (dma_transfer_direction == MM2S_TRANSFER) {
        pthread_mutex_lock(&dma_device->mm2s_cfg_mutex);
        /* clear all the Irq status bits. */
        dma_device->cfg[MM2S_STATUS_REGISTER] = (
                DMASR_IOC_Irq | DMASR_Dly_Irq | DMASR_Err_Irq);

        /* Then reset the channel. */
        dma_device->cfg[MM2S_CONTROL_REGISTER] = DMACR_RESET;
        pthread_mutex_unlock(&dma_device->mm2s_cfg_mutex);

        while (!(dma_device->cfg[MM2S_STATUS_REGISTER] & DMASR_HALTED)) {
            /*
             * Wait for the status register to show the device is halted.
             * At that point, the reset has completed.
             */

            /* Count the number of reset sleeps. */
            reset_delay_count = reset_delay_count + 1;

            if (reset_delay_count == max_reset_delay) {
                /*
                 * Reset has taken too long so assume it has encountered an
                 * error
                 */
                perror("ERROR <DMA Reset Failed> This should have been "
                        "handled already so is a bug.\n"
                        "The system probably needs a reset now.\n");
                exit(1);
            }
            /* Pause so that while() isn't wasting lots of energy looping */
            nanosleep(&reset_sleep, NULL);
        }
    }
    else {

        pthread_mutex_lock(&dma_device->s2mm_cfg_mutex);
        /* clear all the Irq status bits. */
        dma_device->cfg[S2MM_STATUS_REGISTER] = (
                DMASR_IOC_Irq | DMASR_Dly_Irq | DMASR_Err_Irq);

        /* Then reset the channel. */
        dma_device->cfg[S2MM_CONTROL_REGISTER] = DMACR_RESET;
        pthread_mutex_unlock(&dma_device->s2mm_cfg_mutex);

        while (!(dma_device->cfg[S2MM_STATUS_REGISTER] & DMASR_HALTED)) {

            /*
             * Wait for the status register to show the device is halted.
             * At that point, the reset has completed.
             */

            /* Count the number of reset sleeps. */
            reset_delay_count = reset_delay_count + 1;

            if (reset_delay_count == max_reset_delay) {
                /*
                 * Reset has taken too long so assume it has encountered an
                 * error
                 */
                perror("ERROR <DMA Reset Failed>\n");
                exit(1);
            }
            /* Pause so that while() isn't wasting lots of energy looping */
            nanosleep(&reset_sleep, NULL);
        }
    }

    /* Poll the file descriptor to see if a further interrupt was recieved */
    poll_res = poll(fds, 1, 0);
    if (poll_res > 0) {
        /*
         * Reading the file descriptor to clear any interrupts which arrived
         * after resetting the DMA controller.
         */
        read(dma_device->fd, &interrupt_buffer, 4);
    }

    return 0;
}


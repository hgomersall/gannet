# cython: language_level=3
from libc.stdint cimport uintptr_t, uint32_t, int32_t
from libcpp cimport bool
from posix.unistd cimport readlink

cdef extern from "dma.h":

    ctypedef struct dma_device_t:
        pass

    cdef struct dma_sg_descriptor_s:
        uint32_t next_descriptor_ptr
        uint32_t next_descriptor_ptr_msb
        uint32_t buffer_address
        uint32_t buffer_address_msb
        uint32_t reserved_1
        uint32_t reserved_2
        uint32_t control
        uint32_t status
        uint32_t user_application_1
        uint32_t user_application_2
        uint32_t user_application_3
        uint32_t user_application_4

    ctypedef struct dma_scatter_gather_op_t:
        bint valid
        dma_direction direction
        dma_sg_descriptor_s *sg_descriptors
        size_t n_sg_descriptors
        void *mem_ptr
        void *buffer_start
        uintptr_t mem_lower_bound
        uintptr_t mem_upper_bound

    ctypedef enum dma_direction:
        MM2S_TRANSFER = 0
        S2MM_TRANSFER = 1


cdef extern from "gannet.h":

    ctypedef enum:
        GANNET_E_INSIZ = -1
        GANNET_E_INOFF = -2
        GANNET_E_INDIR = -3
        GANNET_E_MEMOVER = -4
        GANNET_E_DMAINT = -5
        GANNET_E_SLAVE = -6
        GANNET_E_DECDE = -7
        GANNET_E_NOSG = -8

    dma_device_t* gannet_device_init(const char *device_path)

    int gannet_device_cleanup(dma_device_t *dma_device)

    int gannet_do_dma(
        dma_device_t *dma_device,
        dma_direction dma_transfer_direction,
        uintptr_t offset,
        uint32_t nbytes)

    int gannet_dma_interrupt_wait(dma_device_t *dma_device,
                                  dma_direction dma_transfer_direction,
                                  int32_t timeout_msec)
    void* gannet_get_memory(dma_device_t *dma_device)

    int  gannet_dma_reset(dma_device_t *dma_device,
                          dma_direction dma_transfer_direction)

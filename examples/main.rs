use std::{
    collections::HashMap,
    fs,
    fs::OpenOptions,
    path::Path,
    path::PathBuf,
    ptr::copy_nonoverlapping,
    ptr::write_volatile,
    sync::atomic::fence,
    sync::atomic::Ordering,
    thread,
    time,
};
use rand::{
    distributions::Uniform,
    Rng,
};

use memmap::{
    MmapOptions,
};

use gannet::{
    uni_channel::direct,
    uni_channel::scatter_gather,
    multi_channel,
};

fn exercise_direct_dma() {

    let s2mm_device_path: PathBuf = ["/dev", "axi_dma_s2mm"].iter().collect();
    let mm2s_device_path: PathBuf = ["/dev", "axi_dma_mm2s"].iter().collect();

    // Create the devices. In the loopback PL the MM2S DMA engine stream
    // output is connected to the S2MM DMA engine stream input.
    let mut s2mm_device = direct::Device::new(&s2mm_device_path).unwrap();
    let mut mm2s_device = direct::Device::new(&mm2s_device_path).unwrap();

    // Get a pointer to and the size of the memory for each DMA device
    let (s2mm_cma, s2mm_cma_size) = s2mm_device.get_memory();
    let (mm2s_cma, mm2s_cma_size) = mm2s_device.get_memory();

    let mut rng = rand::thread_rng();
    let range = Uniform::new(0, 1u16 << 8);

    // Generate valid random values for the size and source & destination
    // offsets of the transfer.
    let nbytes: usize = 8*rng.gen_range(1, (1 << 23)/8);
    let src_offset: usize = 8*rng.gen_range(0, (mm2s_cma_size - nbytes)/8);
    let dest_offset: usize = 8*rng.gen_range(0, (s2mm_cma_size - nbytes)/8);

    // Generate random data
    let src: Vec<u8> =
        (0..nbytes).map(|_| rng.sample(&range) as u8).collect();

    // Create a vector into which we can copy the data out of the destination
    // memory
    let mut dest = vec![1u8; nbytes];
    // Create a vector for a sanity check on the destination memory
    let mut test = vec![0u8; nbytes];

    unsafe {
        // Copy the random data into the source memory
        copy_nonoverlapping(
            src.as_ptr(), mm2s_cma.offset(src_offset as isize), nbytes);
        // Set up the destination memory with known values so we know the
        // state of the memory before the transfer
        copy_nonoverlapping(
            dest.as_ptr(), s2mm_cma.offset(dest_offset as isize), nbytes);
        // Copy the values out of the destination memory into the test vector
        copy_nonoverlapping(
            s2mm_cma.offset(dest_offset as isize), test.as_mut_ptr(), nbytes);
    }

    // Sanity check to make sure the destination memory is known before the
    // transfer. This is so we know any change is cause by the DMA transfer.
    assert!(test == vec![1u8; nbytes]);

    // Trigger the DMA transfers
    mm2s_device.do_dma(&nbytes, &src_offset).unwrap();
    s2mm_device.do_dma(&nbytes, &dest_offset).unwrap();

    let timeout = time::Duration::from_secs(1);

    // Wait for DMA transfers to complete
    mm2s_device.wait_transfer_complete(&timeout).unwrap();
    s2mm_device.wait_transfer_complete(&timeout).unwrap();

    unsafe {
        // Read the destination memory.
        copy_nonoverlapping(
            s2mm_cma.offset(dest_offset as isize), dest.as_mut_ptr(), nbytes);
    }

    mm2s_device.print_status();
    s2mm_device.print_status();

    // Check that we have received the correct data.
    assert!(src == dest);
}

fn exercise_sg_dma() {
    let s2mm_device_path: PathBuf = ["/dev", "axi_dma_sg_s2mm"].iter().collect();
    let mm2s_device_path: PathBuf = ["/dev", "axi_dma_sg_mm2s"].iter().collect();

    // Create the devices. In the loopback PL the MM2S DMA engine stream
    // output is connected to the S2MM DMA engine stream input.
    let mut s2mm_device = scatter_gather::Device::new(&s2mm_device_path).unwrap();
    let mut mm2s_device = scatter_gather::Device::new(&mm2s_device_path).unwrap();

    s2mm_device.print_info();
    mm2s_device.print_info();

    let (mm2s_mem, _mm2s_mem_size) = mm2s_device.get_memory();
    let (s2mm_mem, _s2mm_mem_size) = s2mm_device.get_memory();

    let nbytes = 2560;
    let src_offset = 0;
    let dest_offset = 0;

    let mut rng = rand::thread_rng();
    let range = Uniform::new(0, 1u16 << 8);

    // Create a vector to overwrite the destination before beginning the
    // transfer
    let destination_init_data = vec!(1u8; nbytes);
    // Generate random data
    let test_data: Vec<u8> =
        (0..nbytes).map(|_| rng.sample(&range) as u8).collect();
    // Create a vector into which we can copy the data out of the destination
    // memory
    let mut destination_data = vec!(0u8; nbytes);

    unsafe {
        // Copy the test data into the source memory
        copy_nonoverlapping(
            test_data.as_ptr(), mm2s_mem.offset(src_offset as isize),
            nbytes);
        // Set up the destination memory with known values so we know the
        // state of the memory before the transfer
        copy_nonoverlapping(
            destination_init_data.as_ptr(),
            s2mm_mem.offset(dest_offset as isize), nbytes);
    }

    // Find out how many descriptor chunks are left on the device
    let s2mm_n_avail_desc_chunks =
        s2mm_device.n_available_descriptor_chunks().unwrap();
    let mm2s_n_avail_desc_chunks =
        mm2s_device.n_available_descriptor_chunks().unwrap();

    // Find out how many descriptors are left on the device
    let s2mm_n_avail_descriptors =
        s2mm_device.n_available_descriptors().unwrap();
    let mm2s_n_avail_descriptors =
        mm2s_device.n_available_descriptors().unwrap();

    let n_blocks = 160;
    let block_size = nbytes/n_blocks;
    let block_stride = block_size;

    // Check that we can request the number of blocks specified
    assert!(s2mm_n_avail_descriptors >= n_blocks);
    assert!(mm2s_n_avail_descriptors >= n_blocks);
    assert!(
        s2mm_n_avail_desc_chunks >=
        n_blocks/scatter_gather::descriptors::N_DESCRIPTORS_PER_CHUNK);
    assert!(
        mm2s_n_avail_desc_chunks >=
        n_blocks/scatter_gather::descriptors::N_DESCRIPTORS_PER_CHUNK);

    let mm2s_operation = mm2s_device.new_regular_blocks_operation(
        &block_size, &block_stride, &n_blocks, &src_offset).unwrap();
    let s2mm_operation = s2mm_device.new_regular_blocks_operation(
        &block_size, &block_stride, &n_blocks, &dest_offset).unwrap();

    mm2s_device.do_dma(&mm2s_operation).unwrap();
    s2mm_device.do_dma(&s2mm_operation).unwrap();

    let timeout = time::Duration::from_millis(1000);

    // Wait for DMA transfers to complete
    mm2s_device.wait_transfer_complete(&timeout).unwrap();
    s2mm_device.wait_transfer_complete(&timeout).unwrap();

    // Check and refresh the operations
    mm2s_device.check_and_refresh_operation(&mm2s_operation).unwrap();
    s2mm_device.check_and_refresh_operation(&s2mm_operation).unwrap();

    unsafe {
        // Read the destination memory.
        copy_nonoverlapping(
            s2mm_mem.offset(dest_offset as isize),
            destination_data.as_mut_ptr(), nbytes);
    }

    assert!(destination_data == test_data);
}

fn exercise_mc_dma() {

    let s2mm_device_path: PathBuf = ["/dev", "axi_dma_mc_s2mm"].iter().collect();
    let mm2s_device_path: PathBuf = ["/dev", "axi_dma_mc_mm2s"].iter().collect();

    // Create the devices. In the loopback PL the MM2S DMA engine stream
    // output is connected to the S2MM DMA engine stream input.
    let mut s2mm_device = multi_channel::Device::new(&s2mm_device_path).unwrap();
    let mut mm2s_device = multi_channel::Device::new(&mm2s_device_path).unwrap();

    let (mm2s_mem, _mm2s_mem_size) = mm2s_device.get_memory();
    let (s2mm_mem, _s2mm_mem_size) = s2mm_device.get_memory();

    let nbytes = 128;
    let n_channels = 16;
    let src_offset = 0;
    let dest_offset = 0;

    let mut rng = rand::thread_rng();
    let range = Uniform::new(0, 1u16 << 8);

    // Create a vector to overwrite the destination before beginning the
    // transfer
    let destination_init_data = vec!(1u8; nbytes*n_channels);
    // Generate random data
    let test_data: Vec<u8> =
        (0..nbytes*n_channels).map(|_| rng.sample(&range) as u8).collect();
    // Create a vector into which we can copy the data out of the destination
    // memory
    let mut destination_data = vec!(0u8; nbytes*n_channels);

    unsafe {
        // Copy the test data into the source memory
        copy_nonoverlapping(
            test_data.as_ptr(),
            mm2s_mem.offset(src_offset as isize),
            test_data.len());
        // Set up the destination memory with known values so we know the
        // state of the memory before the transfer
        copy_nonoverlapping(
            destination_init_data.as_ptr(),
            s2mm_mem.offset(dest_offset as isize),
            destination_init_data.len());
    }

    // Find out how many descriptors are left on the device
    let s2mm_n_avail_descriptors =
        s2mm_device.n_available_descriptors().unwrap();
    let mm2s_n_avail_descriptors =
        mm2s_device.n_available_descriptors().unwrap();


    let n_blocks = 4;
    let block_size = nbytes/n_blocks;
    let block_stride = block_size;

    // Check that we can request the number of blocks specified
    assert!(s2mm_n_avail_descriptors >= n_blocks);
    assert!(mm2s_n_avail_descriptors >= n_blocks);

    let mut all_mm2s_operations = vec!();
    let mut all_s2mm_operations = vec!();

    for n in 0..n_channels {

        let operation_src_offset = src_offset + n*nbytes;
        let operation_dest_offset = dest_offset + n*nbytes;

        let mm2s_operation = mm2s_device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks,
            &operation_src_offset).unwrap();
        let s2mm_operation = s2mm_device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks,
            &operation_dest_offset).unwrap();

        all_s2mm_operations.push(s2mm_operation);
        all_mm2s_operations.push(mm2s_operation);
    }

    // Create the operations hashmaps
    let mut mm2s_operations = HashMap::new();
    let mut s2mm_operations = HashMap::new();

    for (n, s2mm_operation) in all_s2mm_operations.iter().enumerate() {
        s2mm_operations.insert(n as u8, s2mm_operation);
    }

    for (n, mm2s_operation) in all_mm2s_operations.iter().enumerate() {
        mm2s_operations.insert(n as u8, mm2s_operation);
    }

    s2mm_device.do_dma(&s2mm_operations).unwrap();
    mm2s_device.do_dma(&mm2s_operations).unwrap();

    let timeout = time::Duration::from_millis(1000);

    // Wait for DMA transfers to complete
    mm2s_device.wait_transfer_complete(&timeout).unwrap();
    s2mm_device.wait_transfer_complete(&timeout).unwrap();

    for s2mm_operation in all_s2mm_operations {
        // Loop over the s2mm operations, check and refresh them
        let s2mm_nbytes_transferred =
            s2mm_device.check_and_refresh_operation(&s2mm_operation).unwrap();
        assert!(s2mm_nbytes_transferred == nbytes);
    }

    for mm2s_operation in all_mm2s_operations {
        // Loop over the s2mm operations, check and refresh them
        let mm2s_nbytes_transferred =
            mm2s_device.check_and_refresh_operation(&mm2s_operation).unwrap();
        assert!(mm2s_nbytes_transferred == nbytes);
    }

    unsafe {
        // Read the destination memory.
        copy_nonoverlapping(
            s2mm_mem.offset(dest_offset as isize),
            destination_data.as_mut_ptr(), nbytes*n_channels);
    }

    assert!(destination_data == test_data);

}

//fn exercise_mc_pl_generator() {
//    // This function requires:
//    // Jackdaw/jackdaw/test_utils/axis_multi_channel_packet_generator
//
//    let generator_device_path: PathBuf = ["/dev", "generator_cntrl"].iter().collect();
//
//    let file = OpenOptions::new()
//        .read(true)
//        .write(true)
//        .open(&generator_device_path).unwrap();
//
//    // Memory map the configuration register
//    let gen_mem = unsafe {
//        MmapOptions::new()
//            .offset(0)
//            .len(4096 as usize)
//            .map_mut(&file).unwrap()
//    };
//
//    // Get a raw pointer to the configuration memory
//    let gen = gen_mem.as_ptr() as *mut u32;
//
//    let s2mm_device_path: PathBuf = ["/dev", "axi_dma_mc_s2mm"].iter().collect();
//
//    // Create the device.
//    let mut s2mm_device = multi_channel::Device::new(&s2mm_device_path).unwrap();
//
//    let (s2mm_mem, _s2mm_mem_size) = s2mm_device.get_memory();
//
//    let n_channels = 16;
//
//    let n_blocks = 4;
//    let block_size = 32;
//    let block_stride = block_size;
//
//    let nbytes = block_size*n_blocks;
//    let dest_offset = 0;
//
//    // Create a vector to overwrite the destination before beginning the
//    // transfer
//    let destination_init_data = vec!(1u8; nbytes*n_channels);
//    // Create a vector into which we can copy the data out of the destination
//    // memory
//    let mut destination_data = vec!(0u8; nbytes*n_channels);
//
//    unsafe {
//        // Set up the destination memory with known values so we know the
//        // state of the memory before the transfer
//        copy_nonoverlapping(
//            destination_init_data.as_ptr(),
//            s2mm_mem.offset(dest_offset as isize),
//            destination_init_data.len());
//    }
//
//    // Find out how many descriptors are left on the device
//    let s2mm_n_avail_descriptors =
//        s2mm_device.n_available_descriptors().unwrap();
//
//    // Check that we can request the number of blocks specified
//    assert!(s2mm_n_avail_descriptors >= n_blocks);
//
//    let mut all_s2mm_operations = vec!();
//
//    for n in 0..n_channels {
//
//        let operation_dest_offset = dest_offset + n*block_stride*n_blocks;
//
//        let s2mm_operation = s2mm_device.new_regular_blocks_operation(
//            &block_size, &block_stride, &n_blocks,
//            &operation_dest_offset).unwrap();
//
//        all_s2mm_operations.push(s2mm_operation);
//    }
//
//    // Create the operations hashmaps
//    let mut s2mm_operations = HashMap::new();
//
//    for (n, s2mm_operation) in all_s2mm_operations.iter().enumerate() {
//
//        s2mm_operations.insert(n as u8, s2mm_operation);
//    }
//
//    s2mm_device.do_dma(&s2mm_operations).unwrap();
//
//    // Pause to let the sytem set up all of the s2mm operations
//    let pause = time::Duration::from_secs(1);
//    thread::sleep(pause);
//
//    let timeout = time::Duration::from_secs(5);
//
//    // Need the memory fences here to prevent the cpu from reordering
//    // the non volatile reads/writes around volatile reads/writes
//    fence(Ordering::SeqCst);
//
//    // Write to the register to trigger data generation
//    unsafe {write_volatile(gen.offset(0), 1u32)};
//
//    fence(Ordering::SeqCst);
//
//    // Wait for DMA transfers to complete
//    s2mm_device.wait_transfer_complete(&timeout).unwrap();
//
//    for s2mm_operation in all_s2mm_operations {
//        // Loop over the s2mm operations, check and refresh them
//        let s2mm_nbytes_transferred =
//            s2mm_device.check_and_refresh_operation(&s2mm_operation).unwrap();
//
//        assert!(s2mm_nbytes_transferred == nbytes);
//    }
//
//    unsafe {
//        // Read the destination memory.
//        copy_nonoverlapping(
//            s2mm_mem.offset(dest_offset as isize),
//            destination_data.as_mut_ptr(), nbytes*n_channels);
//    }
//
//    let mut expected_data = Vec::new();
//
//    for n in 0..n_channels as u8 {
//        for x in 0..n_blocks as u8 {
//            let channel_expected_data = vec![
//                n + 1 + x*n_channels as u8, 0, 0, 0,
//                n + 1 + x*n_channels as u8, 0, 0, 0,
//                n + 1 + x*n_channels as u8, 0, 0, 0,
//                n + 1 + x*n_channels as u8, 0, 0, 0,
//                n + 1 + x*n_channels as u8, 0, 0, 0,
//                n + 1 + x*n_channels as u8, 0, 0, 0,
//                n + 1 + x*n_channels as u8, 0, 0, 0,
//                n + 1 + x*n_channels as u8, 0, 0, 0,];
//            expected_data.extend(channel_expected_data);
//        }
//    }
//
//    println!("##########################");
//    println!("{:?}", &destination_data);
//    println!("##########################");
//    println!("{:?}", &expected_data);
//
//    assert!(destination_data == expected_data);
//
//}

fn main() {
    {
        exercise_direct_dma();
    }
    {
        exercise_sg_dma();
    }
    {
        exercise_mc_dma();
    }
//    {
//        exercise_mc_pl_generator();
//    }
}
